package com.lego.microservices.configuration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.lego.microservices.persistence.Orders;
import com.lego.microservices.persistence.Roles;
import com.lego.microservices.persistence.Users;
import com.lego.microservices.persistence.repository.OrderRepository;
import com.lego.microservices.persistence.repository.RolesRepository;
import com.lego.microservices.persistence.repository.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { RepositoryConfiguration.class })
public class EntityRepositoryTest {

	protected static final Long orderid = 4501081111L;

	protected UserRepository userRepository;
	protected OrderRepository orderRepository;
	protected RolesRepository rolesRepository;

	Orders order = null;

	@Autowired
	public void SetRepositoryTest(UserRepository userRepository, OrderRepository orderRepository,
			RolesRepository rolesRepository) {
		this.userRepository = userRepository;
		this.orderRepository = orderRepository;
		this.rolesRepository = rolesRepository;
	}

	@Test
	public void testOrderfindByOrderId() {

		order = orderRepository.findByOrderId(orderid);
		assertNotNull(order);
		assertEquals((Object) order.getOrderId(), (Object) orderid);

	}

	@Test
	public void testOrderfindBypostatus() {
		List<Orders> orderlist = orderRepository.findBypostatus("Completed");
		assertNotNull(orderlist);
	}

	@Test
	public void testOrderfindByupdatedBy() {
		List<Orders> orderlist = orderRepository.findByupdatedBy("rep1@lego.com");
		assertNotNull(orderlist);
	}

	@Test
	public void testRolefindByRoleId() {
		Roles role = rolesRepository.findByRoleId(1);
		assertNotNull(role);
	}

	@Test
	public void testUserfindByUserId() {
		Users user = userRepository.findByUserId("Premk");
		assertNotNull(user);
	}

}
