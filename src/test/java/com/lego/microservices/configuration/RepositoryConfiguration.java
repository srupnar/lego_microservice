package com.lego.microservices.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableAutoConfiguration
@EntityScan("com.lego.microservices.persistence")
@EnableJpaRepositories("com.lego.microservices.persistence.repository")
@PropertySource("classpath:db-config.properties")

public class RepositoryConfiguration {
}
