/*package com.lego.microservices.services.order;

import java.util.List;
import java.util.logging.Logger;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.lego.microservices.configuration.RepositoryConfiguration;
import com.lego.microservices.persistence.dto.OrderSummaryDto;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { RepositoryConfiguration.class })
@ComponentScan("com.lego.microservices.services.order")
public class OrdersControllerTests //extends AbstractOrdersControllerTests
{
	@Autowired
	OrderController orderController ;


	@Test
	public void getAllOrders() {
		Logger.getGlobal().info("Start getAllOrders test");
		List<OrderSummaryDto> order = orderController.getAllOrders();
		OrderSummaryDto orderSummaryDto = order.get(0);
		Assert.assertNotNull(orderSummaryDto);
		Logger.getGlobal().info("End getAllOrders test");
	}

	@Test
	public void getAllOrdersByAttribute() {
		Logger.getGlobal().info("Start getAllOrdersByAttribute test");
		List<OrderSummaryDto> order = orderController.getAllOrdersByAttribute("SupplierID", "18548");
		OrderSummaryDto orderSummaryDto = order.get(0);
		Assert.assertNotNull(orderSummaryDto);
		Logger.getGlobal().info("End getAllOrdersByAttribute test");
	}

//	@Test
//	public void getGenericSearch() {
//		Logger.getGlobal().info("Start getGenericSearch test");
//		List<GenericOrderSearchDto> order = orderController.getGenericSearch("4501081111");
//		GenericOrderSearchDto genericOrderSearchDto = order.get(0);
//		Assert.assertNotNull(genericOrderSearchDto);
//		Logger.getGlobal().info("End getGenericSearch test");
//	}
//
//	@Test
//	public void getOrdersByID() {
//		Logger.getGlobal().info("Start getOrdersByID test");
//		Long id = 4501081111L;
//		OrderDto order = orderController.getOrdersByID(id);
//		Assert.assertNotNull(order);
//		Logger.getGlobal().info("End getOrdersByID test");
//	}
}
*/