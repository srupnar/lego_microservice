# ROLES TABLE INSERT VALUES
INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(1,"LegoRep", 'repID', 'rep3@lego.com');

INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(2,"Supplier", 'supplierID', '18548');

INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(3,"CustomsBroker", 'customsbroker', 'cus1');

INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(4,"WarehousEmp", 'warehouse_loc', 'billund');

INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(5,"Admin",'', '');

INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(6,"WarehousEmp", 'warehouse_loc', 'hungary');
    
INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(7,"WarehousEmp", 'warehouse_loc', 'Mexico');
    
INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(8,"LegoRep", 'repID', 'rep2@lego.com');
    
INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(9,"LegoRep", 'repID', 'rep3@lego.com');

INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(10,"Supplier", 'supplierID', '121749');
    
INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(11,"Supplier", 'supplierID', 'sup3');

INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(12,"CustomsBroker", 'customsbroker', 'cus2');
    
INSERT INTO ROLES
	(ROLEID,ROLENAME, AttrName, AttrValue) 
	VALUES 
	(13,"CustomsBroker", 'customsbroker', 'cus3');
#**********************************

#USERS TABLE INSERT VALUES
#*************************

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy) 
	VALUES
	("sivap","213123","sdfsdffsdfsdds",1,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy)
	VALUES 
	("premk","2131321","sfasfadsadasd",1,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"system");

INSERT INTO USERS
 	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy)
	VALUES 
	("navanee","788777","kjhjkhkjkhk",2,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy)
 	VALUES 
	("vamsi","788787","aasdadasdasdads",3,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy)
	 VALUES 
	("karuna","2332423","asasdasdasasdasd",4,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy) 
	VALUES 
	("admin","21312","q34242323234",5,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy)
	VALUES
	("navanee1","788777","kjhjkhkjkhk",2,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy) 
	VALUES 
	("vamsi1","788787","aasdadasdasdads",3,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy) 
	VALUES 
	("karuna1","2332423","asasdasdasasdasd",4,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy)
	VALUES 
	("premk1","2131321","sfasfadsadasd",1,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy) 
	VALUES 
	("navanee2","788777","kjhjkhkjkhk",2,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy) 
	VALUES 
	("vamsi2","788787","aasdadasdasdads",3,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");

INSERT INTO USERS 
	(UserID, Salt, Hash, RoleID, LastLoginTime, CreatedTime, CreatedBy, UpdatedTime, UpdatedBy) 
	VALUES 
	("karuna2","2332423","asasdasdasasdasd",4,'2016-11-28 00:00:00','2016-11-28 00:00:00',"System",'2016-11-28 00:00:00',"System");
#**********************************

UPDATE users SET salt ="hz4bWIjfaveTwdEUppbHhonRYSPMYN3x", hash="sCcq7vf+gqe/BWC9fwQ+T8zl" where userid = "sivap";
UPDATE users SET salt ="U8kp66nGL6CuLwcHK3ofe5yK6eepBTGs" , hash="EYHsEAkusa1Q3QmWFHD3WF98" where userid = "premk";
UPDATE users SET salt ="FCtEEzH+FwFWIkFLdqA2glJEzIlK/R4b" , hash="jFotOl8vpdP/4vkcwcsGfVDZ" where userid = "navanee";
UPDATE users SET salt ="0OcpdN8R/iNGz9XgPy5ugEyeAIBUz/Wm" , hash="MbQhR7/T76nLCX82MOzv6pAl" where userid = "vamsi";
UPDATE users SET salt ="z2chksoDf/I31ydwN1VzeGcDssJc00m1" , hash="OrfHGfJaf3UoXuPQ6H/EkBBJ" where userid = "karuna";
UPDATE users SET salt ="3MS4UzGb5whfld7Oqpm/FyV0pQobz0Xv" , hash="ohZ28QZzRDpNmmPYbcAtZhu2" where userid = "admin";
UPDATE users SET salt ="aITmCHJBBWGJvkyJ1ZlJk8HJpKAawZHi" , hash="aUcglOncpb4gM1gGRvrLXrkh" where userid = "navanee1";
UPDATE users SET salt ="xWC0UZNzQ+ge7B2ap9AZ9Grh85KhNKHe" , hash="JW5SOM+sb1i4RfYqabIdCyNq" where userid = "vamsi1";
UPDATE users SET salt ="3pBcWkaGD/+Ys8v7XtHoZKHr0JtnskD3" , hash="WJDid2Oyo1sqBY1dyjNKVjkE" where userid = "karuna1";
UPDATE users SET salt ="Yxj+4lbZ8O0A41qZ4UE36JcpEDA1+kym" , hash="vJd9dIwQ6q0m3n+fJLJlVQpk" where userid = "premk1";
UPDATE users SET salt ="iQc6wiPQ1IMQwAj31cy8G9wrVhHoNL0k" , hash="iGWIgL31KonfZliX8d6nyKc4" where userid = "navanee2";
UPDATE users SET salt ="MIJHnXubLflwNqr6KBLWak5Yq/CPXycv" , hash="NhPZh3p1nnZuRl6AYE/frjSE" where userid = "vamsi2";
UPDATE users SET salt ="rtT9XlNARH/5B15cDMmfj7M/lq7+2FYR" , hash="30GuuO85XJluvsRViZ70d87i" where userid = "karuna2";
#**********************************

#ORDER TABLE INSERT VALUES
#*************************
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (450108111,'With Supplier','rep3@lego.com','2016-11-23 05:30:00','rep1@lego.com','2016-11-22 05:30:00','Billund','Addr1','Add2','Addr1','Addr2','Denmark',18548,'Suzhou','China','','Order is Created',4000,'','$');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501078765,'With Supplier','rep3@lego.com','2016-11-22 00:00:00','rep3@lego.com','2016-11-25 00:00:00','Mexico','Addr1','Add2','Addr1','Addr2','Mexico',121778,'HongKong','HongKong','cus3','Order Received by Supplier',1600,'','$');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501079222,'Delivered','rep2@lego.com','2016-11-23 00:00:00','rep2@lego.com','2016-11-22 00:00:00','Budapest','Addr1','Add2','Addr1','Addr2','Hungary',121749,'Geneva','Switzerland','','Order is Delivered',2400,'','â‚¬');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501079333,'In Warehouse','rep3@lego.com','2016-11-23 00:00:00','rep3@lego.com','2016-11-22 00:00:00','Mexico','Addr1','Add2','Addr1','Addr2','Mexico',121778,'HongKong','HongKong','cus3','Order Received at WareHouse',2000,'','$');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501079465,'Delivered','rep2@lego.com','2016-11-23 00:00:00','rep2@lego.com','2016-11-23 00:00:00','Budapest','Addr1','Add2','Addr1','Addr2','Hungary',121749,'Geneva','Switzerland','','Order is Delivered',2400,'','â‚¬');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501079476,'With Broker','rep3@lego.com','2016-11-23 00:00:00','rep3@lego.com','2016-11-25 00:00:00','Mexico','Addr1','Add2','Addr1','Addr2','Mexico',121778,'HongKong','HongKong','cus3','Order Received by Broker',1200,'','$');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501079555,'New','rep3@lego.com','2016-11-22 00:00:00','rep3@lego.com','2016-11-25 00:00:00','Mexico','Addr1','Add2','Addr1','Addr2','Mexico',121778,'HongKong','HongKong','cus3','Order is Created',800,'','â‚¬');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501079665,'New','rep2@lego.com','2016-11-22 00:00:00','rep2@lego.com','2016-11-23 00:00:00','Budapest','Addr1','Add2','Addr1','Addr2','Hungary',121749,'Geneva','Switzerland','','Order is Created',2000,'','Â¥');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501079666,'New','rep2@lego.com','2016-11-22 00:00:00','rep2@lego.com','2016-11-23 00:00:00','Budapest','Addr1','Add2','Addr1','Addr2','Hungary',121749,'Geneva','Switzerland','','Order is Created',1000,'','$');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501079888,'New','rep2@lego.com','2016-11-22 00:00:00','rep2@lego.com','2016-11-23 00:00:00','Budapest','Addr1','Add2','Addr1','Addr2','Hungary',121749,'Geneva','Switzerland','','Order is Created',600,'','$');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501079983,'Shipped','rep3@lego.com','2016-11-23 00:00:00','rep3@lego.com','2016-11-25 00:00:00','Mexico','Addr1','Add2','Addr1','Addr2','Mexico',121778,'HongKong','HongKong','cus3','Order is Shipped',1200,'','Â¥');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501079985,'Delayed','rep2@lego.com','2016-11-23 00:00:00','rep2@lego.com','2016-11-23 00:00:00','Budapest','Addr1','Add2','Addr1','Addr2','Hungary',121749,'Geneva','Switzerland','','Order is Delayed',2400,'','$');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501081111,'With Supplier','rep3@lego.com','2016-11-23 00:00:00','rep1@lego.com','2016-11-22 00:00:00','Billund','Addr1','Add2','Addr1','Addr2','Denmark',18548,'Suzhou','China','','Order Received by Supplier',4000,'','$');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501081456,'With Supplier','rep3@lego.com','2016-11-23 00:00:00','rep1@lego.com','2016-11-22 00:00:00','Billund','Addr1','Add2','Addr1','Addr2','Denmark',18548,'Suzhou','China','','Order Received by Supplier',3000,'','Â¥');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501081775,'With Supplier','rep1@lego.com','2016-11-22 00:00:00','rep1@lego.com','2016-11-22 00:00:00','Billund','Addr1','Add2','Addr1','Addr2','Denmark',18548,'Suzhou','China','','Order Received by Supplier',2800,'','$');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501081777,'With Supplier','rep1@lego.com','2016-11-22 00:00:00','rep1@lego.com','2016-11-22 00:00:00','Billund','Addr1','Add2','Addr1','Addr2','Denmark',18548,'Suzhou','China','','Order Received by Supplier',2000,'','$');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501081977,'New','rep1@lego.com','2016-11-28 00:00:00','rep1@lego.com','2016-11-28 00:00:00','Billund','Addr1','Add2','Addr1','Addr2','Denmark',18548,'Suzhou','China','','Order is Created',440,'','$');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501081987,'With Supplier','rep3@lego.com','2016-11-23 00:00:00','rep1@lego.com','2016-11-22 00:00:00','Billund','Addr1','Add2','Addr1','Addr2','Denmark',18548,'Suzhou','China','','Order Received by Supplier',3000,'','$');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501081988,'New','rep1@lego.com','2016-11-22 00:00:00','rep1@lego.com','2016-11-22 00:00:00','Billund','Addr1','Add2','Addr1','Addr2','Denmark',18548,'Suzhou','China','','Order is Created',1200,'','$');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501082001,'Completed','rep1@lego.com','2016-11-25 00:00:00','rep1@lego.com','2016-11-22 00:00:00','Billund','Addr1','Add2','Addr1','Addr2','Denmark',18548,'Suzhou','China','','Order is Delivered',10400,'','$');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501082345,'With Supplier','rep1@lego.com','2016-11-22 00:00:00','rep1@lego.com','2016-11-22 00:00:00','Billund','Addr1','Add2','Addr1','Addr2','Denmark',18548,'Suzhou','China','','Order Received by Supplier',3200,'','$');
INSERT INTO ORDERS (OrderID,POStatus,UpdatedBy,UpdatedDate,CreatedBy,CreatedDate,WarehouseCity,SupplierAddress1,SupplierAddress2,WarehouseAddress1,WarehouseAddress2,WarehouseCountry,SupplierId,SupplierCity,SupplierCountry,BrokerId,StatusDescription,PoValue,Description,Currency)  values (4501082346,'With Supplier','rep3@lego.com','2016-11-23 05:30:00','rep1@lego.com','2016-11-22 05:30:00','Billund','Addr1','Add2','Addr1','Addr2','Denmark',123458,'Suzhou','China','','',45676,'','$');

#***********************************
#LINE DETAILS TABLE INSERT VALUES
#*************************
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	10,4501081977,'Billund','596X44x637',5737311,4,'CREATED','Airport Cargo Plane',3456);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	11,4501081988,'Billund','596X44x637',5737311,4,'CREATED','Building Bay',3454656);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	12,4501079888,'Billund','4817007',4817007,4,'CREATED','Toyz Newborn',456);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	15,4501081777,'Billund','596X44x637',5737311,4,'CREATED','Airport Cargo Plane',567);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	16,4501081775,'Billund','596X44x637',5737311,4,'RECEIVEDBYSUPPLIER','Fire Ladder Truck',2345);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	17,4501081775,'Billund','600X455X37',5737312,4,'RECEIVEDBYSUPPLIER','Toyz Newborn',345);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	18,4501079555,'Mexico','596X44x638',4817008,4,'CREATED','Airport Cargo Plane',2456);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	19,4501079665,'Billund','4817007',4817007,4,'CREATED','Toyz Newborn',245);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	20,4501081977,'Billund','600X455X37',5737312,4,'CREATED','Building Bay',332);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	21,4501081988,'Billund','600X455X37',573731,4,'CREATED','Airport Cargo Plane',232);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	25,4501081777,'Billund','600X455X37',573731,4,'CREATED','Toyz Newborn',233);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	32,4501079666,'Billund','4817007',4817007,4,'CREATED','Building Bay',323);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	35,4501081456,'Billund','596X44x637',5737311,4,'RECEIVEDBYSUPPLIER','Airport Cargo Plane',3323);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	37,4501081456,'Billund','600X455X37',5737312,4,'RECEIVEDBYSUPPLIER','Fire Ladder Truck',1212);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	39,4501079465,'Billund','4817007',4817007,4,'CREATED','Toyz Newborn',12121);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	40,4501079476,'Mexico','596X44x638',4817008,4,'RECEIVEDBYBROKER','Airport Cargo Plane',33);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	51,4501082345,'Billund','596X44x637',5737311,4,'RECEIVEDBYSUPPLIER','Building Bay',323);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	52,4501082345,'Billund','600X455X37',5737312,4,'RECEIVEDBYSUPPLIER','Fire Ladder Truck',232323);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	54,4501078765,'Mexico','596X44x638',4817008,4,'RECEIVEDBYSUPPLIER','Airport Cargo Plane',332);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	61,4501081987,'Billund','596X44x637',5737311,4,'RECEIVEDBYSUPPLIER','Toyz Newborn',323);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	62,4501081987,'Billund','600X455X37',5737312,4,'RECEIVEDBYSUPPLIER','Fire Ladder Truck',2332323);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	65,4501079985,'Billund','4817007',4817007,4,'CREATED','Building Bay',323);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	66,4501079983,'Mexico','596X44x638',4817008,4,'SHIPPED','Airport Cargo Plane',0);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	71,4501081111,'Billund','596X44x637',5737311,4,'RECEIVEDBYSUPPLIER','Fire Ladder Truck',0);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	73,4501081111,'Billund','600X455X37',5737312,4,'RECEIVEDBYSUPPLIER','Building Bay',323);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	75,4501079222,'Billund','4817007',4817007,4,'CREATED','Airport Cargo Plane',323);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	77,4501079333,'Mexico','596X44x638',4817008,4,'RECEIVEDATAWAREHOUSE','Toyz Newborn',3232);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	91,4501082001,'Billund','595X45x636',5737312,4,'Completed','Fire Ladder Truck',600);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	93,4501082001,'Billund','599X47x638',5737313,4,'Completed','Toyz Newborn',800);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	94,4501082001,'Billund','599X48x789',5737314,4,'Completed','Building Bay',700);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	95,4501082346,'Billund','637',637,4,'RECEIVEDBYSUPPLIER','Building Bay',323);
INSERT INTO linedetails (LineID, OrderID,CreatedAt,MouldNo,ArtNo,Quantity,Status,LineDescription,UnitPrice ) VALUES (	96,4501082346,'Billund','596X44x637',5737311,4,'Created','Fire Ladder Truck',23);

#*******************************************
#ORDER STATUS TABLE INSERT VALUES
#*************************
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (1,4501078765,54,'Created','rep1@lego.com','2016-11-25 05:30:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (2,4501078765,54,'Shipped','Supplieruserid','2016-11-28 04:00:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (3,4501078765,54,'Received','Customerbrokeruserid','2016-11-28 09:00:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (4,4501078765,54,'Received','Warehouseuserid','2016-11-29 06:06:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (5,4501078765,54,'Received','Supplieruserid','2016-11-30 08:00:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (6,4501079222,75,'Created','rep1@lego.com','2016-11-22 03:20:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (7,4501079222,75,'Received','Supplieruserid','2016-11-24 09:00:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (8,4501079222,75,'Received','Customerbrokeruserid','2016-11-25 09:00:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (9,4501079222,75,'Shipped','Supplieruserid','2016-11-25 09:00:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (10,4501079222,75,'Delivered','Warehouseuserid','2016-11-26 09:00:00','','','Order is Delivered');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (11,4501079333,77,'Created','rep1@lego.com','2016-11-22 03:20:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (12,4501079333,77,'Received','Supplieruserid','2016-11-22 03:20:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (13,4501079333,77,'Received','Customerbrokeruserid','2016-11-22 04:30:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (14,4501079333,77,'Shipped','Supplieruserid','2016-11-22 04:30:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (15,4501079333,77,'Received','Warehouseuserid','2016-11-22 08:30:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (16,4501079465,39,'Created','rep1@lego.com','2016-11-23 07:00:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (17,4501079465,39,'Received','Supplieruserid','2016-11-24 12:30:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (18,4501079465,39,'Received','Customerbrokeruserid','2016-11-25 08:17:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (19,4501079465,39,'Shipped','Supplieruserid','2016-11-25 08:17:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (20,4501079465,39,'Delivered','Warehouseuserid','2016-11-26 08:17:00','','','Order is Delivered');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (21,4501079476,40,'Received','Customerbrokeruserid','2016-11-22 03:20:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (22,4501079476,40,'Created','rep1@lego.com','2016-11-22 07:00:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (23,4501079476,40,'Received','Supplieruserid','2016-11-22 07:00:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (24,4501079476,40,'With Broker','Customerbrokeruserid','2016-11-22 07:00:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (25,4501079555,18,'Created','rep1@lego.com','2016-11-25 05:30:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (26,4501079665,19,'Created','rep1@lego.com','2016-11-23 01:30:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (27,4501079666,32,'Created','rep1@lego.com','2016-11-23 01:30:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (28,4501079888,12,'Created','rep1@lego.com','2016-11-28 01:30:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (29,4501079983,66,'Created','rep1@lego.com','2016-11-25 08:00:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (30,4501079983,66,'Received','Supplieruserid','2016-11-26 08:00:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (31,4501079983,66,'Received','Customerbrokeruserid','2016-11-28 12:30:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (32,4501079983,66,'Received','Warehouseuserid','2016-11-29 00:30:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (33,4501079983,66,'Shipped','Supplieruserid','2016-11-29 12:30:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (34,4501079985,65,'Created','rep1@lego.com','2016-11-23 08:00:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (35,4501079985,65,'Received','Supplieruserid','2016-11-24 12:30:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (36,4501079985,65,'Shipped','Supplieruserid','2016-11-25 12:30:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (37,4501079985,65,'Received','Warehouseuserid','2016-11-26 12:30:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (38,4501079985,65,'Delayed','Supplieruserid','2016-11-27 12:30:00','','','Order is Delayed');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (39,4501081111,71,'Created','rep1@lego.com','2016-11-22 03:20:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (40,4501081111,73,'Created','rep1@lego.com','2016-11-22 03:20:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (41,4501081111,71,'Received','Customerbrokeruserid','2016-11-29 08:17:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (42,4501081111,71,'Shipped','Supplieruserid','2016-11-29 08:17:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (43,4501081111,73,'Received','Customerbrokeruserid','2016-11-30 08:17:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (44,4501081111,73,'Shipped','Supplieruserid','2016-11-30 08:17:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (45,4501081111,71,'Received','Warehouseuserid','2016-12-01 08:17:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (46,4501081111,73,'Received','Warehouseuserid','2016-12-01 08:17:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (47,4501081111,71,'Received','Supplieruserid','2016-12-24 03:20:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (48,4501081111,73,'Received','Supplieruserid','2016-12-28 03:20:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (49,4501081456,35,'Created','rep1@lego.com','2016-11-22 07:00:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (50,4501081456,37,'Created','rep1@lego.com','2016-11-22 07:00:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (51,4501081456,35,'Received','Customerbrokeruserid','2016-11-25 12:30:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (52,4501081456,35,'Shipped','Supplieruserid','2016-11-25 12:30:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (53,4501081456,35,'Received','Warehouseuserid','2016-11-26 12:30:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (54,4501081456,37,'Received','Customerbrokeruserid','2016-11-29 12:30:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (55,4501081456,37,'Shipped','Supplieruserid','2016-11-29 12:30:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (56,4501081456,37,'Received','Warehouseuserid','2016-11-30 12:30:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (57,4501081456,35,'Received','Supplieruserid','2016-12-24 07:00:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (58,4501081456,37,'Received','Supplieruserid','2016-12-28 07:00:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (59,4501081775,16,'Created','rep1@lego.com','2016-11-22 01:30:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (60,4501081775,16,'Received','Customerbrokeruserid','2016-11-25 07:30:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (61,4501081775,16,'Shipped','Supplieruserid','2016-11-25 07:30:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (62,4501081775,16,'Received','Warehouseuserid','2016-11-26 07:30:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (63,4501081775,16,'Received','Supplieruserid','2016-12-24 05:30:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (64,4501081775,17,'Created','rep1@lego.com','2016-11-22 01:30:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (65,4501081775,17,'Received','Customerbrokeruserid','2016-11-25 07:30:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (66,4501081775,17,'Shipped','Supplieruserid','2016-11-25 07:30:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (67,4501081775,17,'Received','Warehouseuserid','2016-11-26 07:30:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (68,4501081775,17,'Received','Supplieruserid','2016-12-24 05:30:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (69,4501081977,20,'Created','rep1@lego.com','2016-11-28 01:30:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (70,4501081977,10,'Created','rep1@lego.com','2016-12-29 04:30:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (71,4501081777,15,'Created','rep1@lego.com','2016-11-22 01:30:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (72,4501081777,25,'Created','rep1@lego.com','2016-11-22 01:30:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (73,4501081777,15,'Received','Customerbrokeruserid','2016-11-25 07:30:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (74,4501081777,15,'Shipped','Supplieruserid','2016-11-25 07:30:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (75,4501081777,15,'Received','Warehouseuserid','2016-11-26 07:30:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (76,4501081777,25,'Received','Customerbrokeruserid','2016-11-26 07:30:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (77,4501081777,25,'Shipped','Supplieruserid','2016-11-26 07:30:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (78,4501081777,25,'Received','Warehouseuserid','2016-11-27 07:30:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (79,4501081777,15,'Received','Supplieruserid','2016-12-24 01:30:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (80,4501081777,25,'Received','Supplieruserid','2016-12-24 01:30:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (81,4501081987,62,'Created','rep1@lego.com','2016-11-23 08:00:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (82,4501081987,61,'Received','Customerbrokeruserid','2016-11-25 08:17:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (83,4501081987,61,'Shipped','Supplieruserid','2016-11-25 08:17:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (84,4501081987,61,'Received','Warehouseuserid','2016-11-26 08:17:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (85,4501081987,62,'Received','Customerbrokeruserid','2016-11-26 08:17:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (86,4501081987,62,'Shipped','Supplieruserid','2016-11-26 08:17:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (87,4501081987,62,'Received','Warehouseuserid','2016-11-27 08:17:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (88,4501081987,61,'Received','Supplieruserid','2016-12-24 08:00:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (89,4501081987,62,'Received','Supplieruserid','2016-12-24 08:00:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (90,4501081988,11,'Created','rep1@lego.com','2016-11-28 01:30:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (91,4501081988,21,'Created','rep1@lego.com','2016-11-28 01:30:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (92,4501082001,91,'Created','rep1@lego.com','2016-11-25 09:00:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (93,4501082001,93,'Created','rep1@lego.com','2016-11-25 09:00:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (94,4501082001,94,'Created','rep1@lego.com','2016-11-25 09:00:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (95,4501082001,91,'Delayed','Supplieruserid','2016-11-27 09:00:00','','','Order is Delayed');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (96,4501082001,91,'Received','Supplieruserid','2016-11-27 09:00:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (97,4501082001,91,'Shipped','Supplieruserid','2016-11-27 09:00:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (98,4501082001,93,'Delayed','Supplieruserid','2016-11-27 09:00:00','','','Order is Delayed');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (99,4501082001,93,'Received','Supplieruserid','2016-11-27 09:00:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (100,4501082001,93,'Shipped','Supplieruserid','2016-11-27 09:00:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (101,4501082001,94,'Delayed','Supplieruserid','2016-11-27 09:00:00','','','Order is Delayed');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (102,4501082001,94,'Received','Supplieruserid','2016-11-27 09:00:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (103,4501082001,94,'Shipped','Supplieruserid','2016-11-27 09:00:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (104,4501082001,91,'Received','Warehouseuserid','2016-11-28 09:00:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (105,4501082001,93,'Received','Warehouseuserid','2016-11-28 09:00:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (106,4501082001,91,'Received','Customerbrokeruserid','2016-11-29 09:00:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (107,4501082001,93,'Received','Customerbrokeruserid','2016-11-29 09:00:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (108,4501082001,94,'Received','Customerbrokeruserid','2016-11-29 09:00:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (109,4501082001,94,'Received','Warehouseuserid','2016-11-29 09:00:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (110,4501082001,94,'Completed','Warehouseuserid','2016-11-30 09:00:00','','','Order is Delivered');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (111,4501082001,93,'Completed','Warehouseuserid','2016-12-01 09:00:00','','','Order is Delivered');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (112,4501082001,91,'Completed','Warehouseuserid','2016-12-02 09:00:00','','','Order is Delivered');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (113,4501082345,51,'Created','rep1@lego.com','2016-11-22 05:30:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (114,4501082345,52,'Created','rep1@lego.com','2016-11-22 05:30:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (115,4501082345,51,'Received','Customerbrokeruserid','2016-11-25 07:30:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (116,4501082345,51,'Shipped','Supplieruserid','2016-11-25 07:30:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (117,4501082345,52,'Shipped','Supplieruserid','2016-11-25 07:30:00','','','Order is Shipped');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (118,4501082345,51,'Received','Warehouseuserid','2016-11-26 07:30:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (119,4501082345,52,'Received','Customerbrokeruserid','2016-11-26 07:30:00','','','Order Received by Broker');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (120,4501082345,52,'Received','Warehouseuserid','2016-11-27 07:30:00','','','Order Received at WareHouse');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (121,4501082345,51,'Received','Supplieruserid','2016-11-28 05:30:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (122,4501082345,52,'Received','Supplieruserid','2016-11-28 05:30:00','','','Order Received by Supplier');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (123,4501082346,95,'Created','rep1@lego.com','2016-11-22 05:30:00','','','Order is Created');
INSERT INTO ORDERSTATUS(Id,OrderID,LineID,Status,CreatedBy,CreatedDate,AttrName,AttrValue,Description) values (124,4501082346,96,'Created','rep1@lego.com','2016-11-22 05:30:00','','','Order is Created');

#*******************************
#SHIPMENT DETAILS TABLE INSERT VALUES
#*************************
insert into shipmentdetails (TrackingID,'OrderID','LineID','ShippingAgency','LastStatus','UpdatedDate','UpdatedAt','linedetails','orders','UpdatedBy') values
(123123423,4501081111,71,'sds','DELIVERED','2016-11-22 00:00:00','2016-11-22','','','');
