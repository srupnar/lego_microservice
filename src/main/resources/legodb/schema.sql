drop table if exists Users ;
drop table if exists roles ;
drop table if exists ShipmentDetails;
drop table if exists OrderStatus;
drop table if exists LineDetails;
drop table if exists Orders;


CREATE TABLE `roles` (
  `RoleID` int(11) NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(200) DEFAULT NULL,
  `AttrName` varchar(200) DEFAULT NULL,
--  `AttrValue` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

CREATE TABLE `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(40) NOT NULL,
  `Salt` varchar(200) DEFAULT NULL,
  `Hash` varchar(200) DEFAULT NULL,
  `RoleID` int(11) DEFAULT NULL,
  `LastLoginTime` datetime DEFAULT NULL,
  `CreatedTime` datetime DEFAULT NULL,
  `CreatedBy` varchar(100) DEFAULT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `RoleValue` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `RoleID` (`RoleID`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`RoleID`) REFERENCES `roles` (`RoleID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

CREATE TABLE `orders` (
  `OrderID` bigint(20) NOT NULL AUTO_INCREMENT,
  `POStatus` varchar(200) DEFAULT NULL,
  `StatusDescription` varchar(200) DEFAULT NULL,
  `POValue` double DEFAULT NULL,
  `Currency` varchar(45) DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `PartNo` int(11) DEFAULT NULL,
  `MouldNo` int(11) DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `BrokerId` varchar(200) DEFAULT NULL,
  `SupplierId` int(11) DEFAULT NULL,
  `SupplierName` varchar(200) DEFAULT NULL,
  `SupplierAddress` varchar(200) DEFAULT NULL,
  `SupplierAddress1` varchar(200) DEFAULT NULL,
  `SupplierAddress2` varchar(200) DEFAULT NULL,
  `SupplierCity` varchar(200) DEFAULT NULL,
  `SupplierCountry` varchar(200) DEFAULT NULL,
  `WarehouseAddress` varchar(200) DEFAULT NULL,
  `WarehouseAddress1` varchar(200) DEFAULT NULL,
  `WarehouseAddress2` varchar(200) DEFAULT NULL,
  `WarehouseCity` varchar(200) DEFAULT NULL,
  `WarehouseCountry` varchar(200) DEFAULT NULL,
  `ExpectedDeliveryDate` datetime DEFAULT NULL,
  `CreatedAt` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(200) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`OrderID`)
) ENGINE=InnoDB AUTO_INCREMENT=4501082356 DEFAULT CHARSET=utf8;

CREATE TABLE `linedetails` (
  `LineID` int(11) NOT NULL AUTO_INCREMENT,
  `OrderID` bigint(20) NOT NULL,
  `CreatedAt` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(200) DEFAULT NULL,
  `ArtNo` int(11) DEFAULT NULL,
  `LineDescription` varchar(200) DEFAULT NULL,
  `MouldNo` varchar(200) DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `Status` varchar(200) DEFAULT NULL,
  `UnitPrice` double DEFAULT NULL,
  #PRIMARY KEY (`LineID`, `OrderID`),
  KEY `OrderID` (`OrderID`),
  CONSTRAINT uc_OrderLineID UNIQUE (`LineID`,`OrderID`),
  CONSTRAINT `linedetails_ibfk_1` FOREIGN KEY (`OrderID`) REFERENCES `orders` (`OrderID`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8;

CREATE TABLE `orderstatus` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `OrderID` bigint(20) DEFAULT NULL,
  `LineID` int(11) DEFAULT NULL,
  `Status` varchar(100) DEFAULT NULL,
  `CreatedBy` varchar(200) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `AttrName` varchar(200) DEFAULT NULL,
  `AttrValue` varchar(200) DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `OrderID` (`OrderID`),
  KEY `LineID` (`LineID`),
  CONSTRAINT `orderstatus_ibfk_1` FOREIGN KEY (`OrderID`) REFERENCES `orders` (`OrderID`),
  CONSTRAINT `orderstatus_ibfk_2` FOREIGN KEY (`LineID`) REFERENCES `linedetails` (`LineID`)
) ENGINE=InnoDB AUTO_INCREMENT=29318 DEFAULT CHARSET=utf8;

CREATE TABLE `shipmentdetails` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TrackingID` int(11) DEFAULT NULL,
  `OrderID` bigint(20) DEFAULT NULL,
  `LineID` int(11) DEFAULT NULL,
  `ShippingAgency` varchar(200) DEFAULT NULL,
  `LastStatus` varchar(200) DEFAULT NULL,
  `UpdatedDate` datetime DEFAULT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedAt` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `OrderID` (`OrderID`),
  KEY `LineID` (`LineID`),
  CONSTRAINT `shipmentdetails_ibfk_1` FOREIGN KEY (`OrderID`) REFERENCES `orders` (`OrderID`),
  CONSTRAINT `shipmentdetails_ibfk_2` FOREIGN KEY (`LineID`) REFERENCES `linedetails` (`LineID`)
) ENGINE=InnoDB AUTO_INCREMENT=78739 DEFAULT CHARSET=utf8;
