package com.lego.microservices.services.roles;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lego.microservices.exceptions.CannotPerformOperationException;
import com.lego.microservices.exceptions.InvalidHashException;
import com.lego.microservices.exceptions.RoleNotFoundException;
import com.lego.microservices.exceptions.UserNotFoundException;
import com.lego.microservices.persistence.Roles;
import com.lego.microservices.persistence.Users;
import com.lego.microservices.persistence.dto.RolesDto;
import com.lego.microservices.persistence.repository.RolesRepository;
import com.lego.microservices.persistence.repository.UserRepository;

/**
 * A RESTFul controller for accessing user information.
 * 
 * @author Suresh.Rupnar
 * 
 */
@RestController
public class RolesController {

	protected Logger logger = Logger.getLogger(RolesController.class.getName());
	protected RolesRepository rolesRepository;
	protected UserRepository userRepository;

	/**
	 * Create an instance plugging in the repository of Role and Users.
	 * 
	 * @param roleRepository
	 *            An user repository implementation.
	 */
	@Autowired
	public RolesController(RolesRepository RolesRepository, UserRepository userrepository) {
		this.rolesRepository = RolesRepository;
		this.userRepository = userrepository;

	}
	
	/**
	 * Fetch role details by role ID
	 * @param roleID
	 * @return Roles
	 * @throws CannotPerformOperationException
	 * @throws InvalidHashException
	 */
	@RequestMapping("/roles/{roleID}")
	public RolesDto getRoleDetails(@PathVariable("roleID") int roleID)
			throws CannotPerformOperationException, InvalidHashException {

		logger.info("roles-service getRoleDetails(Integer roleID) invoked: " + roleID);
		Roles role = rolesRepository.findByRoleId(roleID);
		
		if(role == null) {
			throw new RoleNotFoundException(roleID);
		}

		return fillRolesDto(role);
	}
	
	/**
	 * Fetch role details by user Id
	 * @param userId
	 * @return Roles
	 * @throws CannotPerformOperationException
	 * @throws InvalidHashException
	 */
	@RequestMapping("/roles/user/{userId}")
	public RolesDto getRoleDetails(@PathVariable("userId") String userId)
			throws CannotPerformOperationException, InvalidHashException {

		logger.info("roles-service getRoleDetails(Integer roleID) invoked: " + userId);
		Users users = userRepository.findByUserId(userId);
		
		if(users == null) {
			throw new UserNotFoundException(userId);
		}
		
		RolesDto rolesDto = fillRolesDto(users.getRoles());
		rolesDto.setAttrValue(users.getRoleValue());
		return rolesDto;
	}
	
	private RolesDto fillRolesDto(Roles roles) {
		RolesDto rolesDto = new RolesDto();
		rolesDto.setAttrName(roles.getAttrName());
//		rolesDto.setAttrValue(roles.getAttrValue());
		rolesDto.setRoleId(roles.getRoleId());
		rolesDto.setRoleName(roles.getAttrName());
		
		return rolesDto;
	}

}
