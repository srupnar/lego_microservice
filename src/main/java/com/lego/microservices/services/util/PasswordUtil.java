package com.lego.microservices.services.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.xml.bind.DatatypeConverter;

import com.lego.microservices.exceptions.CannotPerformOperationException;
import com.lego.microservices.exceptions.InvalidHashException;

/**
 * This utility is useful for validating user
 * 
 * @author Suresh.Rupnar
 *
 */
public class PasswordUtil {

	public static final String PBKDF2_ALGORITHM = "PBKDF2WithHmacSHA1";

	// These constants may be changed without breaking existing hashes.
	public static final int SALT_BYTE_SIZE = 24;
	public static final int HASH_BYTE_SIZE = 18;
	public static final int PBKDF2_ITERATIONS = 64000;

	// These constants define the encoding and may not be changed.
	public static final int HASH_SECTIONS = 5;
	public static final int HASH_ALGORITHM_INDEX = 0;
	public static final int ITERATION_INDEX = 1;
	public static final int HASH_SIZE_INDEX = 2;
	public static final int SALT_INDEX = 3;
	public static final int PBKDF2_INDEX = 4;

	/**
	 * This function creates Hash value for given password
	 * 
	 * @param password
	 * @return
	 * @throws CannotPerformOperationException
	 */
	public static String createHash(String password) throws CannotPerformOperationException {
		return createHash(password.toCharArray());
	}

	/**
	 * This function is useful for user creation, where we are persisting Hash
	 * and Salt value of provided password by Base64 algorithm
	 * 
	 * @param password
	 * @return
	 * @throws CannotPerformOperationException
	 */
	public static String getHashSalt(char[] password) throws CannotPerformOperationException {
		// Generate a random salt
		SecureRandom random = new SecureRandom();
		byte[] salt = new byte[SALT_BYTE_SIZE];
		random.nextBytes(salt);

		// Hash the password
		byte[] hash = pbkdf2(password, salt, PBKDF2_ITERATIONS, HASH_BYTE_SIZE);
		
		return toBase64(salt) + "$*$" + toBase64(hash);
	}

	/**
	 * This function will return Hash value w.r.t given string token as password
	 * by using Base64 algorithm
	 * 
	 * @param password
	 * @return
	 * @throws CannotPerformOperationException
	 */
	public static String createHash(char[] password) throws CannotPerformOperationException {
		// Generate a random salt
		SecureRandom random = new SecureRandom();
		byte[] salt = new byte[SALT_BYTE_SIZE];
		random.nextBytes(salt);

		// Hash the password
		byte[] hash = pbkdf2(password, salt, PBKDF2_ITERATIONS, HASH_BYTE_SIZE);
		int hashSize = hash.length;

		// format: algorithm:iterations:hashSize:salt:hash
		System.out.println("salt :" + toBase64(salt) + "   hash :" + toBase64(hash));
		String parts = "sha1:" + PBKDF2_ITERATIONS + ":" + hashSize + ":" + toBase64(salt) + ":" + toBase64(hash);
		return parts;
	}

	/**
	 * Verify password by given password. Here, we have used Base64 algorithm
	 * for Hash and Salt computation and comparison
	 * 
	 * @param password
	 * @param Hash
	 * @param salt
	 * @return
	 * @throws CannotPerformOperationException
	 * @throws InvalidHashException
	 */
	public static boolean verifyPassword(char[] password, String Hash, String salt)
			throws CannotPerformOperationException, InvalidHashException {

		byte[] salt1 = null;
		try {
			salt1 = fromBase64(salt);
		} catch (IllegalArgumentException ex) {
			throw new InvalidHashException("Base64 decoding of salt failed.", ex);
		}

		byte[] hash1 = null;
		try {
			hash1 = fromBase64(Hash);
		} catch (IllegalArgumentException ex) {
			throw new InvalidHashException("Base64 decoding of pbkdf2 output failed.", ex);
		}

		// Compute the hash of the provided password, using the same salt,
		// iteration count, and hash length
		byte[] testHash = pbkdf2(password, salt1, PBKDF2_ITERATIONS, hash1.length);
		// Compare the hashes in constant time. The password is correct if
		// both hashes match.
		return slowEquals(hash1, testHash);
	}

	private static boolean slowEquals(byte[] a, byte[] b) {
		int diff = a.length ^ b.length;
		for (int i = 0; i < a.length && i < b.length; i++)
			diff |= a[i] ^ b[i];
		return diff == 0;
	}

	private static byte[] pbkdf2(char[] password, byte[] salt, int iterations, int bytes)
			throws CannotPerformOperationException {
		try {
			PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, bytes * 8);
			SecretKeyFactory skf = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM);
			return skf.generateSecret(spec).getEncoded();
		} catch (NoSuchAlgorithmException ex) {
			throw new CannotPerformOperationException("Hash algorithm not supported.", ex);
		} catch (InvalidKeySpecException ex) {
			throw new CannotPerformOperationException("Invalid key spec.", ex);
		}
	}

	private static byte[] fromBase64(String hex) throws IllegalArgumentException {
		return DatatypeConverter.parseBase64Binary(hex);
	}

	private static String toBase64(byte[] array) {
		return DatatypeConverter.printBase64Binary(array);
	}

	/*
	 * public static void main(String[] args) {
	 * System.out.println(getHashSalt("radhan".toCharArray())); }
	 */
}
