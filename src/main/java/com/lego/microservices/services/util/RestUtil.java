/**
 * 
 */
package com.lego.microservices.services.util;

import org.springframework.http.HttpStatus;

/**
 * This is utility function for error handling
 * @author Suresh.Rupnar
 *
 */
public class RestUtil {
	
	/**
	 * 
	 * @param status
	 * @return
	 */
	public static boolean isError(HttpStatus status) {
		HttpStatus.Series series = status.series();
		return (HttpStatus.Series.CLIENT_ERROR.equals(series) || HttpStatus.Series.SERVER_ERROR.equals(series));
	}

}
