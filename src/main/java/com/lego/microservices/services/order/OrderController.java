package com.lego.microservices.services.order;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lego.microservices.exceptions.GenericException;
import com.lego.microservices.exceptions.NoOrderDetailsException;
import com.lego.microservices.persistence.Linedetails;
import com.lego.microservices.persistence.Orders;
import com.lego.microservices.persistence.Orderstatus;
import com.lego.microservices.persistence.Shipmentdetails;
import com.lego.microservices.persistence.dto.GenericOrderSearchDto;
import com.lego.microservices.persistence.dto.LineIdDto;
import com.lego.microservices.persistence.dto.LinedetailsDto;
import com.lego.microservices.persistence.dto.OrderCreationDto;
import com.lego.microservices.persistence.dto.OrderDto;
import com.lego.microservices.persistence.dto.OrderSummaryDto;
import com.lego.microservices.persistence.dto.OrderUpdateDto;
import com.lego.microservices.persistence.dto.OrderstatusDto;
import com.lego.microservices.persistence.dto.ShippingOrderDto;
import com.lego.microservices.persistence.dto.StaticsDto;
import com.lego.microservices.persistence.dto.StaticsDtoMain;
import com.lego.microservices.persistence.dto.UpdateOrderDto;
import com.lego.microservices.persistence.repository.LinedetailsRepository;
import com.lego.microservices.persistence.repository.OrderRepository;
import com.lego.microservices.persistence.repository.OrderStatusRepository;
import com.lego.microservices.persistence.repository.RolesRepository;

/**
 * A RESTFul controller for accessing user information.
 * 
 * @author Suresh.Rupnar
 * 
 */
@RestController
@Transactional
public class OrderController {

	@PersistenceContext
	private EntityManager em;

	protected Logger logger = Logger.getLogger(OrderController.class.getName());
	protected OrderRepository orderRepository;
	protected RolesRepository rolesRepository;
	protected OrderStatusRepository orderStatusRepository;
	protected LinedetailsRepository linedetailsRepository;

	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	/**
	 * Create an instance plugging in the respository of Users.
	 * 
	 * @param userRepository
	 *            An user repository implementation.
	 */
	@Autowired
	public OrderController(OrderRepository orderRepository, RolesRepository rolesRepository,
			OrderStatusRepository orderStatusRepository, LinedetailsRepository linedetailsRepository) {
		this.orderRepository = orderRepository;
		this.rolesRepository = rolesRepository;
		this.orderStatusRepository = orderStatusRepository;
		this.linedetailsRepository = linedetailsRepository;

		logger.info("OrderRepository, LinedetailsRepository and RolesRepository called");
	}

	/**
	 * Fetch all orders from database
	 * 
	 * @return
	 */
	@RequestMapping("/orders")
	public List<OrderSummaryDto> getAllOrders() {
		logger.info("order-service getAllOrders() invoked: ");
		List<Orders> orders = orderRepository.findAll();

		List<OrderSummaryDto> orderSummaryDtoList = new ArrayList<OrderSummaryDto>();
		for (Orders orders2 : orders) {
			OrderSummaryDto orderSummaryDto = new OrderSummaryDto();
			orderSummaryDto.setOrderId(orders2.getOrderId());
			orderSummaryDto.setStatus(orders2.getPostatus());
			orderSummaryDto.setDescription(orders2.getStatusDescription());
			orderSummaryDto.setExpectedDeliveryDate(orders2.getExpectedDeliveryDate() == null ? null
					: dateFormat.format(orders2.getExpectedDeliveryDate()));
			orderSummaryDtoList.add(orderSummaryDto);
		}

		if (orderSummaryDtoList.isEmpty())
			throw new NoOrderDetailsException("");
		return orderSummaryDtoList;
	}

	/**
	 * Fetch list of all orders filtered by Role Attribute name and value
	 * 
	 * @return
	 */
	@RequestMapping("/orders/{roleAttrName}/{roleAttrValue:.+}")
	public List<OrderSummaryDto> getAllOrdersByAttribute(@PathVariable("roleAttrName") String roleAttrName,
			@PathVariable("roleAttrValue") String roleAttrValue) {

		logger.info("order-service getAllOrdersByAttribute(roleAttrName1,roleAttrName2) invoked: ");
		List<Orders> orders = new ArrayList<Orders>();
		if (roleAttrName.equalsIgnoreCase("postatus"))
			orders = orderRepository.findBypostatus(roleAttrValue);
		else if (roleAttrName.equalsIgnoreCase("updatedBy"))
			orders = orderRepository.findByupdatedBy(roleAttrValue);
		else if (roleAttrName.equalsIgnoreCase("createdBy"))
			orders = orderRepository.findBycreatedBy(roleAttrValue);
		else if (roleAttrName.equalsIgnoreCase("warehouseCity"))
			orders = orderRepository.findBywarehouseCity(roleAttrValue);
		else if (roleAttrName.equalsIgnoreCase("supplierAddress1"))
			orders = orderRepository.findBysupplierAddress1(roleAttrValue);
		else if (roleAttrName.equalsIgnoreCase("warehouseAddress2"))
			orders = orderRepository.findBywarehouseAddress2(roleAttrValue);
		else if (roleAttrName.equalsIgnoreCase("warehouseCountry"))
			orders = orderRepository.findBywarehouseCountry(roleAttrValue);
		else if (roleAttrName.equalsIgnoreCase("supplierId"))
			orders = orderRepository.findBysupplierId(Integer.parseInt(roleAttrValue.toLowerCase()));
		else if (roleAttrName.equalsIgnoreCase("supplierCountry"))
			orders = orderRepository.findBysupplierCountry(roleAttrValue);
		else if (roleAttrName.equalsIgnoreCase("brokerId"))
			orders = orderRepository.findBybrokerId(roleAttrValue);

		List<OrderSummaryDto> orderSummaryDtoList = new ArrayList<OrderSummaryDto>();
		for (Orders orders2 : orders) {
			OrderSummaryDto orderSummaryDto = new OrderSummaryDto();
			orderSummaryDto.setOrderId(orders2.getOrderId());
			orderSummaryDto.setStatus(orders2.getPostatus());
			orderSummaryDto.setDescription(orders2.getStatusDescription());
			orderSummaryDto.setExpectedDeliveryDate(orders2.getExpectedDeliveryDate() == null ? null
					: dateFormat.format(orders2.getExpectedDeliveryDate()));
			orderSummaryDtoList.add(orderSummaryDto);
		}

		if (orderSummaryDtoList.isEmpty())
			throw new NoOrderDetailsException("");
		return orderSummaryDtoList;
	}

	/**
	 * Fetch order details by order ID
	 * 
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping("/orders/{orderId}")
	public OrderDto getOrdersByID(@PathVariable("orderId") long orderId) {
		logger.info("order-service getOrdersByID() invoked: ");
		Orders order = orderRepository.findByOrderId(orderId);

		if (order == null) {
			throw new NoOrderDetailsException("");
		}

		OrderDto orderDto = new OrderDto();
		orderDto.setBrokerId(order.getBrokerId());
		orderDto.setCreatedBy(order.getCreatedBy());
		orderDto.setCreatedDate(dateFormat.format(order.getCreatedDate()));
		orderDto.setOrderId(order.getOrderId());
		orderDto.setPostatus(order.getPostatus());
		orderDto.setSupplierAddress1(order.getSupplierAddress1());
		orderDto.setSupplierAddress2(order.getSupplierAddress2());
		orderDto.setSupplierCity(order.getSupplierCity());
		orderDto.setSupplierCountry(order.getSupplierCountry());
		orderDto.setSupplierId(order.getSupplierId());
		orderDto.setUpdatedBy(order.getUpdatedBy());
		orderDto.setUpdatedDate(dateFormat.format(order.getUpdatedDate()));
		orderDto.setWarehouseAddress1(order.getWarehouseAddress1());
		orderDto.setWarehouseAddress2(order.getWarehouseAddress2());
		orderDto.setWarehouseCity(order.getWarehouseCity());
		orderDto.setWarehouseCountry(order.getWarehouseCountry());
		orderDto.setPoValue(order.getPoValue());
		orderDto.setStatusdescription(order.getStatusDescription());
		orderDto.setCurrency(order.getCurrency());
		orderDto.setExpectedDeliveryDate(
				order.getExpectedDeliveryDate() == null ? null : dateFormat.format(order.getExpectedDeliveryDate()));
		orderDto.setSupplierName(order.getSupplierName());

		Set<LinedetailsDto> linedetailsSet = new HashSet();

		Set<Linedetails> linedetails = order.getLinedetailses();
		for (Linedetails linedetails1 : linedetails) {
			LinedetailsDto linedetailsDto = new LinedetailsDto();
			linedetailsDto.setArtNo(linedetails1.getArtNo());
			linedetailsDto.setCreatedAt(linedetails1.getCreatedAt());
			linedetailsDto.setLineId(linedetails1.getLineId());
			linedetailsDto.setMouldNo(linedetails1.getMouldNo());
			linedetailsDto.setQuantity(linedetails1.getQuantity());
			linedetailsDto.setStatus(linedetails1.getStatus());
			linedetailsDto.setUnitPrice(linedetails1.getUnitPrice());
			linedetailsDto.setLineDescription(linedetails1.getLineDescription());

			List<Orderstatus> orderstatus = orderStatusRepository.findbyOrderStatus(order.getOrderId(),
					linedetails1.getLineId());
			Set<OrderstatusDto> orderstatusesSet = new HashSet();
			for (Orderstatus orderstatus2 : orderstatus) {
				OrderstatusDto orderstatuDto = new OrderstatusDto();
				orderstatuDto.setId(orderstatus2.getId());
				orderstatuDto.setOrderId(orderstatus2.getOrders().getOrderId());
				orderstatuDto.setLineId(orderstatus2.getLinedetails().getLineId());
				orderstatuDto.setAttrName(orderstatus2.getAttrName());
				orderstatuDto.setAttrValue(orderstatus2.getAttrValue());
				orderstatuDto.setCreatedBy(orderstatus2.getCreatedBy());
				orderstatuDto.setCreatedDate(dateFormat.format(orderstatus2.getCreatedDate()));
				orderstatuDto.setDescription(orderstatus2.getDescription());
				orderstatuDto.setStatus(orderstatus2.getStatus());

				orderstatusesSet.add(orderstatuDto);
			}

			linedetailsDto.setOrderStatusesDto(orderstatusesSet);
			linedetailsSet.add(linedetailsDto);
		}
		orderDto.setLinedetailsesDto(linedetailsSet);

		return orderDto;
	}

	/**
	 * Fetch list of all orders basis on orderId, mouldNo and artNo
	 * 
	 * @return List<Object>, if order found. Else error message
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping("/search/{roleAttrName}/{roleAttrValue}/{genericAttribute}")
	public List<GenericOrderSearchDto> getGenericSearch(@PathVariable("roleAttrName") String roleAttrName,
			@PathVariable("roleAttrValue") String roleAttrValue,
			@PathVariable("genericAttribute") String genericAttribute) {
		logger.info("order-service getGenericSearch() invoked: ");
		Map<String, String> orderColumn = new HashMap();
		orderColumn.put("orderid", "orderId");
		orderColumn.put("postatus", "postatus");
		orderColumn.put("updatedby", "updatedBy");
		orderColumn.put("updateddate", "updatedDate");
		orderColumn.put("createdby", "createdBy");
		orderColumn.put("createddate", "createdDate");
		orderColumn.put("warehousecity", "warehouseCity");
		orderColumn.put("supplieraddress1", "supplierAddress1");
		orderColumn.put("supplieraddress2", "supplierAddress2");
		orderColumn.put("warehouseaddress1", "warehouseAddress1");
		orderColumn.put("warehouseaddress2", "warehouseAddress2");
		orderColumn.put("warehousecountry", "warehouseCountry");
		orderColumn.put("supplierid", "supplierId");
		orderColumn.put("suppliercity", "supplierCity");
		orderColumn.put("suppliercountry", "supplierCountry");
		orderColumn.put("brokerid", "brokerId");
		orderColumn.put("statusdescription", "statusDescription");
		orderColumn.put("povalue", "poValue");
		orderColumn.put("description", "description");
		orderColumn.put("currency", "currency");
		orderColumn.put("expecteddeliverydate", "expectedDeliveryDate");

		List<Object[]> resultList = em
				.createQuery(
						"Select distinct ord.orderId,ord.postatus, ord.statusDescription,ord.expectedDeliveryDate from Orders ord, Linedetails line where ord.orderId = line.orders.orderId and ord."
								+ orderColumn.get(roleAttrName.toLowerCase()) + " = '" + roleAttrValue
								+ "'and (ord.orderId like '%" + genericAttribute + "%' or line.mouldNo like '%"
								+ genericAttribute + "%' or line.artNo like '%" + genericAttribute + "%')")
				.getResultList();

		if (resultList.isEmpty()) {
			throw new NoOrderDetailsException("");
		}

		List<GenericOrderSearchDto> genericOrderSearcchDtoList = new ArrayList();
		GenericOrderSearchDto genericOrderSearcchDto = null;
		for (Object[] row : resultList) {
			genericOrderSearcchDto = new GenericOrderSearchDto();
			genericOrderSearcchDto.setOrderId(Long.parseLong(row[0].toString()));
			genericOrderSearcchDto.setStatus(row[1] == null ? "" : row[1].toString());
			genericOrderSearcchDto.setDescription(row[2] == null ? "" : row[2].toString());
			genericOrderSearcchDto.setExpectedDeliveryDate(row[3] == null ? "" : dateFormat.format(row[3]).toString());
			genericOrderSearcchDtoList.add(genericOrderSearcchDto);
		}

		return genericOrderSearcchDtoList;
	}

	/**
	 * Create Order based on provided JSON data for order creation
	 * 
	 * @param order,
	 *            JSON object
	 * @return Order creation message. Else error message
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(method = RequestMethod.POST, path = "/createorder", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
	public OrderCreationDto createOrder(@RequestBody Orders order) {
		logger.info("order-service createOrder(Orders order) invoked: ");

		OrderCreationDto orderCreationDto = new OrderCreationDto();
		List<Integer> lineId = new ArrayList();

		// Insert Order
		order.setStatusDescription("Order is Created");
		order.setPostatus("New");
		order.setCreatedDate(new Date());
		em.persist(order);
		em.flush();

		Set<Linedetails> lindetailsList = order.getLinedetailses();
		for (Linedetails linedetails : lindetailsList) {
			// Insert Line details
			linedetails.setOrders(order);
			linedetails.setStatus("Created");

			// Insert Order status
			Set<Orderstatus> orderstatusList = new HashSet();
			for (Orderstatus orderstatus : linedetails.getOrderstatuses()) {
				orderstatus.setStatus("Created");
				orderstatus.setDescription("Order is Created");
				orderstatus.setCreatedDate(new Date());
				orderstatus.setOrders(order);
				orderstatus.setLinedetails(linedetails);

				orderstatusList.add(orderstatus);
			}
			linedetails.setOrderstatuses(orderstatusList);
			em.persist(linedetails);

			lineId.add(linedetails.getLineId());
		}
		em.flush();

		logger.info("order-service createOrder(Orders order) found: ");
		orderCreationDto.setOrderId(order.getOrderId());
		orderCreationDto.setExpectedDeliveryDate(
				order.getExpectedDeliveryDate() == null ? null : dateFormat.format(order.getExpectedDeliveryDate()));
		orderCreationDto.setLineId(lineId);

		return orderCreationDto;
	}

	/**
	 * Update order by order status
	 * 
	 * @param orderstatus
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(method = RequestMethod.POST, path = "/updateorder", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
	public OrderUpdateDto updateOrder(@RequestBody UpdateOrderDto updateOrderDto) {
		logger.info("order-service updateOrder(Orders order) invoked: ");

		try {			
			// Order
			Orders orderss = orderRepository.findByOrderId(updateOrderDto.getOrders().longValue());
			orderss.setPostatus(updateOrderDto.getStatus());
			orderss.setStatusDescription(updateOrderDto.getStatusDescription());
			orderss.setDescription(updateOrderDto.getDescription());

			Set<Linedetails> linedetails = orderss.getLinedetailses();
			for (Linedetails linedetails2 : linedetails) {
				// check given Line Id
				Set<Orderstatus> orderstatuSet1 = new HashSet();
				if (updateOrderDto.getLinedetails().equals(linedetails2.getLineId())) {					
					// Line Details
					linedetails2.setStatus(updateOrderDto.getStatus());
					linedetails2.setLineDescription(updateOrderDto.getDescription());
					
					// Order status
					Orderstatus orderstatus = new Orderstatus();
					orderstatus.setAttrName(updateOrderDto.getAttrName());
					orderstatus.setAttrValue(updateOrderDto.getAttrValue());
					orderstatus.setCreatedBy(updateOrderDto.getCreatedBy());
					orderstatus.setDescription(updateOrderDto.getDescription());
					orderstatus.setStatus(updateOrderDto.getStatus());
					orderstatus.setCreatedDate(new Date());
					
					orderstatus.setOrders(orderss);
					orderstatus.setLinedetails(linedetails2);
					
					orderstatuSet1.add(orderstatus);

					// Shipment details
					Shipmentdetails shipmentdetails = updateOrderDto.getShipmentdetails();
					Set<Shipmentdetails> shipmentdetails1 = new HashSet();
					if (shipmentdetails != null) {
						shipmentdetails.setLinedetails(linedetails2);
						shipmentdetails.setOrders(orderss);
						
						shipmentdetails1.add(shipmentdetails);
//						em.persist(shipmentdetails);
					}
					linedetails2.setShipmentdetailses(shipmentdetails1);
					linedetails2.setOrderstatuses(orderstatuSet1);
					
					em.persist(linedetails2);
					break;
				}
			}			
			em.persist(orderss);
		} catch (Exception e) {
			throw new GenericException("", e);
		}

		// After successful order update, prepare response DTO
		OrderUpdateDto orderUpdateDto = new OrderUpdateDto();
		orderUpdateDto.setLineId(updateOrderDto.getLinedetails().toString());
		orderUpdateDto.setOrderId(updateOrderDto.getOrders().toString());
		if (updateOrderDto.getShipmentdetails() != null) {
			orderUpdateDto.setShipmentTrackingId(updateOrderDto.getShipmentdetails().getTrackingId());
		}
		return orderUpdateDto;

	}

	/**
	 * update order by Line items, shipping details, order and order status
	 * 
	 * @param order
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(method = RequestMethod.POST, path = "/updateshipping", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
	public ShippingOrderDto updateShippingOrder(@RequestBody Orders order) {
		logger.info("order-service updateShippingOrder(Orders order) invoked: ");

		Set<LineIdDto> lineIdDtoset = new HashSet();

		// retrieve Order persistent entity by order Id
		Orders ordertowork = em.find(Orders.class, order.getOrderId());

		if (ordertowork == null)
			throw new NoOrderDetailsException("");

		try {
			// update order entity
			ordertowork.setStatusDescription(order.getStatusDescription());
			ordertowork.setPostatus(order.getPostatus());
			ordertowork.setUpdatedBy(order.getUpdatedBy());
			ordertowork.setUpdatedDate(order.getUpdatedDate());

			Set<Linedetails> linedetailSet = order.getLinedetailses();
			for (Linedetails linedetails : linedetailSet) {
				LineIdDto lineIdDto = new LineIdDto();

				// retrieve Line Details persistent entity by Line Id and Order
				// ID
				Linedetails lndetails = linedetailsRepository.findbyLinedetails(ordertowork.getOrderId(),
						linedetails.getLineId());
				lndetails.setStatus(linedetails.getStatus());

				lineIdDto.setLineId(linedetails.getLineId());

				// Order Status
				Set<Orderstatus> orderstatusSet = linedetails.getOrderstatuses();
				Set<Orderstatus> orderstatuSet1 = new HashSet();
				for (Orderstatus orderstatus : orderstatusSet) {
					orderstatus.setLinedetails(lndetails);
					orderstatus.setOrders(ordertowork);

					orderstatuSet1.add(orderstatus);
				}

				// Shipment details
				Set<Shipmentdetails> shipmentdetailsSet = linedetails.getShipmentdetailses();
				Set<Shipmentdetails> shipmentdetails1 = new HashSet();
				for (Shipmentdetails shipmentdetails : shipmentdetailsSet) {
					shipmentdetails.setLinedetails(lndetails);
					shipmentdetails.setOrders(ordertowork);

					lineIdDto.setTrackingId(shipmentdetails.getTrackingId());
					shipmentdetails1.add(shipmentdetails);
				}
				lndetails.setShipmentdetailses(shipmentdetails1);
				lndetails.setOrderstatuses(orderstatuSet1);

				em.persist(lndetails);
				lineIdDtoset.add(lineIdDto);
			}
			em.persist(ordertowork);
			em.flush();
		} catch (Exception e) {
			throw new GenericException("", e);
		}

		// Response DTO, after successful saving
		ShippingOrderDto shippingOrderDto = new ShippingOrderDto();
		shippingOrderDto.setOrderId(order.getOrderId());
		shippingOrderDto.setLinedetailsesDto(lineIdDtoset);

		return shippingOrderDto;
	}

	/**
	 * Fetch Weekly/Monthly order status count
	 * 
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(method = RequestMethod.GET, path = "/getStatistics/{roleAttrName}/{roleAttrValue:.+}", produces = MediaType.APPLICATION_JSON)
	public List<StaticsDtoMain> getStatistics(@PathVariable("roleAttrName") String roleAttrName,
			@PathVariable("roleAttrValue") String roleAttrValue) {
		List<StaticsDtoMain> staticsDtoMainlist = new ArrayList();

		logger.info("order-service getStatistics() invoked: ");

		String filter = getFilteredOrderColumn(roleAttrName, roleAttrValue);

		try {
			NumberFormat formatter = new DecimalFormat("#0.00");

			Map<String, Integer> today = new HashMap();
			List<Object[]> todaylist = em.createQuery(
					"SELECT ord.postatus,count(*) FROM Orders ord WHERE YEAR(ord.createdDate) = YEAR(NOW()) AND MONTH(ord.createdDate) = MONTH(NOW()) AND DAY(ord.createdDate) = DAY(NOW()) "
							+ filter + " group by ord.postatus order by ord.postatus desc")
					.getResultList();

			for (Object[] row : todaylist) {
				today.put(row[0] == null ? "NA" : row[0].toString(), Integer.parseInt(row[1].toString()));
			}

			double todaytotal = 0;
			double todaycompleted = today.containsKey("Completed") ? today.get("Completed") : 0;
			double todaydelayed = today.containsKey("Delayed") ? today.get("Delayed") : 0;

			for (String key : today.keySet()) {
				todaytotal = todaytotal + today.get(key);
			}

			if (Math.floor(todaytotal) != 0) {
				StaticsDtoMain staticsDtoMaintoday = new StaticsDtoMain();
				List<StaticsDto> staticsDtoListtoday = new ArrayList();
				staticsDtoMaintoday.setDate("Today");
				double todayonschedule = todaytotal - (todaycompleted + todaydelayed);
				double todayonschedulepercent = (todayonschedule * 100) / todaytotal;
				double todaycompletedpercent = (todaycompleted * 100) / todaytotal;
				double todaydelayedpercent = (todaydelayed * 100) / todaytotal;

				StaticsDto dtoOnscheduledtoday = new StaticsDto();
				dtoOnscheduledtoday.setCount(todayonschedule);
				dtoOnscheduledtoday.setPercentage(formatter.format(todayonschedulepercent));
				dtoOnscheduledtoday.setStatus("Onscheduled");
				staticsDtoListtoday.add(dtoOnscheduledtoday);

				StaticsDto dtoCompletedtoday = new StaticsDto();
				dtoCompletedtoday.setCount(todaycompleted);
				dtoCompletedtoday.setPercentage(formatter.format(todaycompletedpercent));
				dtoCompletedtoday.setStatus("Completed");
				staticsDtoListtoday.add(dtoCompletedtoday);

				StaticsDto dtoDelayedtoday = new StaticsDto();
				dtoDelayedtoday.setCount(todaydelayed);
				dtoDelayedtoday.setPercentage(formatter.format(todaydelayedpercent));
				dtoDelayedtoday.setStatus("Delayed");
				staticsDtoListtoday.add(dtoDelayedtoday);

				staticsDtoMaintoday.setStaticsDto(staticsDtoListtoday);
				staticsDtoMainlist.add(staticsDtoMaintoday);
			}

			Map<String, Integer> currentweek = new HashMap();
			List<Object[]> currentweeklist = em.createQuery(
					"SELECT ord.postatus, count(*) FROM Orders ord WHERE WEEKOFYEAR(ord.createdDate) = WEEKOFYEAR(NOW()) "
							+ filter + "  group by ord.postatus order by ord.postatus desc")
					.getResultList();

			for (Object[] row : currentweeklist) {
				currentweek.put(row[0] == null ? "NA" : row[0].toString(), Integer.parseInt(row[1].toString()));
			}

			double currentweektotal = 0;
			double currentweekcompleted = currentweek.containsKey("Completed") ? currentweek.get("Completed") : 0;
			double currentweekdelayed = currentweek.containsKey("Delayed") ? currentweek.get("Delayed") : 0;

			for (String key : currentweek.keySet()) {
				currentweektotal = currentweektotal + currentweek.get(key);
			}

			if (Math.floor(currentweektotal) != 0) {
				List<StaticsDto> staticsDtoListWeekly = new ArrayList();
				StaticsDtoMain staticsDtoMaincurrentweek = new StaticsDtoMain();
				staticsDtoMaincurrentweek.setDate("Weekly");
				double currentweekonschedule = currentweektotal - (currentweekcompleted + currentweekdelayed);
				double currentweekonschedulepercent = (currentweekonschedule * 100) / currentweektotal;
				double currentweekcompletedpercent = (currentweekcompleted * 100) / currentweektotal;
				double currentweekdelayedpercent = (currentweekdelayed * 100) / currentweektotal;

				StaticsDto dtoOnscheduledWeekly = new StaticsDto();
				dtoOnscheduledWeekly.setCount(currentweekonschedule);
				dtoOnscheduledWeekly.setPercentage(formatter.format(currentweekonschedulepercent));
				dtoOnscheduledWeekly.setStatus("Onscheduled");
				staticsDtoListWeekly.add(dtoOnscheduledWeekly);

				StaticsDto dtoCompletedWeekly = new StaticsDto();
				dtoCompletedWeekly.setCount(currentweekcompletedpercent);
				dtoCompletedWeekly.setPercentage(formatter.format(currentweekcompletedpercent));
				dtoCompletedWeekly.setStatus("Completed");
				staticsDtoListWeekly.add(dtoCompletedWeekly);

				StaticsDto dtoDelayedWeekly = new StaticsDto();
				dtoDelayedWeekly.setCount(currentweekdelayedpercent);
				dtoDelayedWeekly.setPercentage(formatter.format(currentweekdelayed));
				dtoDelayedWeekly.setStatus("Delayed");
				staticsDtoListWeekly.add(dtoDelayedWeekly);

				staticsDtoMaincurrentweek.setStaticsDto(staticsDtoListWeekly);
				staticsDtoMainlist.add(staticsDtoMaincurrentweek);
			}

			Map<String, Integer> currentmonth = new HashMap();
			List<Object[]> currentmonthlist = em.createQuery(
					"SELECT ord.postatus, count(*) FROM Orders ord WHERE YEAR(ord.createdDate) = YEAR(NOW()) AND MONTH(ord.createdDate) = MONTH(NOW()) "
							+ filter + " group by ord.postatus order by ord.postatus desc")
					.getResultList();

			for (Object[] row : currentmonthlist) {
				currentmonth.put(row[0] == null ? "NA" : row[0].toString(), Integer.parseInt(row[1].toString()));
			}

			double currentmonthtotal = 0;
			double currentmonthcompleted = currentmonth.containsKey("Completed") ? currentmonth.get("Completed") : 0;
			double currentmonthdelayed = currentmonth.containsKey("Delayed") ? currentmonth.get("Delayed") : 0;

			for (String key : currentmonth.keySet()) {
				currentmonthtotal = currentmonthtotal + currentmonth.get(key);
			}

			if (Math.floor(currentmonthtotal) != 0) {

				StaticsDtoMain staticsDtoMaincurrentmonth = new StaticsDtoMain();
				staticsDtoMaincurrentmonth.setDate("Monthly");

				double currentmonthonschedule = currentmonthtotal - (currentmonthcompleted + currentmonthdelayed);
				double currentmonthonschedulepercent = (currentmonthonschedule * 100) / currentmonthtotal;
				double currentmonthcompletedpercent = (currentmonthcompleted * 100) / currentmonthtotal;
				double currentmonthdelayedpercent = (currentmonthdelayed * 100) / currentmonthtotal;

				List<StaticsDto> staticsDtoListMonthly = new ArrayList();

				StaticsDto dtoOnscheduledMonthly = new StaticsDto();
				dtoOnscheduledMonthly.setCount(currentmonthonschedule);
				dtoOnscheduledMonthly.setPercentage(formatter.format(currentmonthonschedulepercent));
				dtoOnscheduledMonthly.setStatus("Onscheduled");
				staticsDtoListMonthly.add(dtoOnscheduledMonthly);

				StaticsDto dtoCompletedMonthly = new StaticsDto();
				dtoCompletedMonthly.setCount(currentmonthcompleted);
				dtoCompletedMonthly.setPercentage(formatter.format(currentmonthcompletedpercent));
				dtoCompletedMonthly.setStatus("Completed");
				staticsDtoListMonthly.add(dtoCompletedMonthly);

				StaticsDto dtoDelayedMonthly = new StaticsDto();
				dtoDelayedMonthly.setCount(currentmonthdelayed);
				dtoDelayedMonthly.setPercentage(formatter.format(currentmonthdelayedpercent));
				dtoDelayedMonthly.setStatus("Delayed");
				staticsDtoListMonthly.add(dtoDelayedMonthly);

				staticsDtoMaincurrentmonth.setStaticsDto(staticsDtoListMonthly);
				staticsDtoMainlist.add(staticsDtoMaincurrentmonth);
			}
		} catch (Exception e) {
			logger.info("order-service getStatistics() Exception : " + e);
			throw new GenericException("", e);
		}
		return staticsDtoMainlist;
	}

	private String getFilteredOrderColumn(String roleAttrName, String roleAttrValue) {

		String orderColumnFilter = "";

		if (roleAttrName.equalsIgnoreCase("admin") && roleAttrValue.equalsIgnoreCase("admin")) {
			return orderColumnFilter;
		} else {
			if (roleAttrName.equalsIgnoreCase("postatus"))
				orderColumnFilter = "AND postatus = '" + roleAttrValue + "'";
			else if (roleAttrName.equalsIgnoreCase("updatedBy"))
				orderColumnFilter = "AND updatedBy = '" + roleAttrValue + "'";
			else if (roleAttrName.equalsIgnoreCase("createdBy"))
				orderColumnFilter = "AND createdBy = '" + roleAttrValue + "'";
			else if (roleAttrName.equalsIgnoreCase("warehouseCity"))
				orderColumnFilter = "AND warehouseCity = '" + roleAttrValue + "'";
			else if (roleAttrName.equalsIgnoreCase("supplierAddress1"))
				orderColumnFilter = "AND supplierAddress1 = '" + roleAttrValue + "'";
			else if (roleAttrName.equalsIgnoreCase("warehouseAddress2"))
				orderColumnFilter = "AND warehouseAddress2 = '" + roleAttrValue + "'";
			else if (roleAttrName.equalsIgnoreCase("warehouseCountry"))
				orderColumnFilter = "AND warehouseCountry = '" + roleAttrValue + "'";
			else if (roleAttrName.equalsIgnoreCase("supplierId"))
				orderColumnFilter = "AND supplierId = " + roleAttrValue;
			else if (roleAttrName.equalsIgnoreCase("supplierCountry"))
				orderColumnFilter = "AND supplierCountry = '" + roleAttrValue + "'";
			else if (roleAttrName.equalsIgnoreCase("brokerId"))
				orderColumnFilter = "AND brokerId = '" + roleAttrValue + "'";

			return orderColumnFilter;
		}
	}
}
