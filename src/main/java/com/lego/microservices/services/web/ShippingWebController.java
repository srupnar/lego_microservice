/**
 * 
 */
package com.lego.microservices.services.web;

import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import com.lego.microservices.msg.Messages;
import com.lego.microservices.persistence.dto.ErrorDto;

/**
 * Client controller, fetches Shipping Tracking details from the microservice via
 * {@link ShippingWebService}.
 * 
 * @author Suresh.Rupnar
 * 
 */
@RestController
public class ShippingWebController {
	
	@Autowired
	protected ShippingWebService shippingWebService;

	protected Logger logger = Logger.getLogger(ShippingWebController.class.getName());

	public ShippingWebController(ShippingWebService shippingWebService) {
		this.shippingWebService = shippingWebService;
	}
	
	/**
	 * Fetch list of all shipping details filtered by Tracking Id
	 * 
	 * @return
	 */
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/shippingdetails/{trackingId}", produces = MediaType.APPLICATION_JSON)
	public Object getShippingDetails(@PathVariable("trackingId") String trackingId) {

		Object orderDto = null;
		try {
			logger.info("ShippingWeb-service getShippingDetails(String trackingId) invoked");

			orderDto = shippingWebService.getShippingDetails(trackingId);
			logger.info("ShippingWeb-service getShippingDetails(String trackingId) found");
		} catch (HttpClientErrorException e) {
			logger.info("ShippingWeb-service getShippingDetails(String trackingId) exception : " + e);
			return new ErrorDto(Messages.getMessage("errorCode_TechnicalError"),
					Messages.getMessage("errorMessage_TechnicalError"));
		}

		return orderDto;
	}

}
