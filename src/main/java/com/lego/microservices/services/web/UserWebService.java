/**
 * 
 */
package com.lego.microservices.services.web;

import java.io.IOException;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lego.microservices.exceptions.GenericException;
import com.lego.microservices.exceptions.util.ExceptionHandlerResource;
import com.lego.microservices.persistence.dto.ErrorDto;
import com.lego.microservices.persistence.dto.UsersDto;
import com.lego.microservices.services.util.MyResponseErrorHandler;
import com.lego.microservices.services.util.RestUtil;

/**
 * Hide the access to the microservice inside this local service.
 * 
 * @author Suresh.Rupnar
 * 
 */
@Service
public class UserWebService {

	@Autowired
	@LoadBalanced
	protected RestTemplate restTemplate;

	protected String serviceUrl;
	private ObjectMapper objectMapper;

	protected Logger logger = Logger.getLogger(UserWebService.class.getName());

	/**
	 * default Constructor
	 * 
	 * @param serviceUrl
	 */
	public UserWebService(String serviceUrl) {
		this.serviceUrl = serviceUrl.startsWith("http") ? serviceUrl : "http://" + serviceUrl;
		objectMapper = new ObjectMapper();
	}

	/**
	 * The RestTemplate works because it uses a custom request-factory that uses
	 * Ribbon to look-up the service to use. This method simply exists to show
	 * this.
	 */
	@PostConstruct
	public void demoOnly() {
		// Can't do this in the constructor because the RestTemplate injection
		// happens afterwards.
		logger.warning("The RestTemplate request factory is " + restTemplate.getRequestFactory().getClass());
	}

	/**
	 * Validate user by given user Id and password
	 * 
	 * @param userID
	 * @param password
	 * @return user details, if success. Else error message
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object validateUser(String JsonData) {

		logger.info("Calling Users web service validateUser(String userID, String password) REST microservice");

		restTemplate.setErrorHandler(new MyResponseErrorHandler());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<String> request = new HttpEntity(JsonData, headers);
		ResponseEntity<String> response = restTemplate.exchange(serviceUrl + "/users/validate", HttpMethod.POST,
				request, String.class);
		String responseBody = response.getBody();
		try {
			if (RestUtil.isError(response.getStatusCode())) {
				ExceptionHandlerResource error = objectMapper.readValue(responseBody, ExceptionHandlerResource.class);
				return new ErrorDto(error.getMessage().substring(0, 3), error.getMessage().substring(4));
			} else {
				return objectMapper.readValue(responseBody, UsersDto.class);
			}
		} catch (IOException e) {
			throw new GenericException("", e);
		}
	}

	/**
	 * Validate user by given user Id and password
	 * 
	 * @param userID
	 * @param password
	 * @return user details, if success. Else error message
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object createuser(String JsonData) {

		logger.info("Calling Users web service validateUser(String userID, String password) REST microservice");

		restTemplate.setErrorHandler(new MyResponseErrorHandler());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<String> request = new HttpEntity(JsonData, headers);
		ResponseEntity<String> response = restTemplate.exchange(serviceUrl + "/createuser", HttpMethod.POST, request,
				String.class);
		String responseBody = response.getBody();
		try {
			if (RestUtil.isError(response.getStatusCode())) {
				ExceptionHandlerResource error = objectMapper.readValue(responseBody, ExceptionHandlerResource.class);
				return new ErrorDto(error.getMessage().substring(0, 3), error.getMessage().substring(4));
			} else {
				return objectMapper.readValue(responseBody, UsersDto.class);
			}
		} catch (IOException e) {
			throw new GenericException("", e);
		}
	}
}
