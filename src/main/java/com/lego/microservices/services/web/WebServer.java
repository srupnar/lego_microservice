/**
 * 
 */
package com.lego.microservices.services.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

/**
 * User web-server. Works as a microservice client, fetching data from the
 * Users-Service. Uses the Discovery Server (Eureka) to find the microservice.
 * 
 * @author Suresh.Rupnar
 * 
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
@EnableDiscoveryClient
@ComponentScan(useDefaultFilters = false) // Disable component scanner
public class WebServer {

	/**
	 * URL uses the logical name of users-service - upper or lower case, doesn't
	 * matter.
	 */
	public static final String USERS_SERVICE_URL = "http://USERS-SERVICE";
	
	/**
	 * URL uses the logical name of users-service - upper or lower case, doesn't
	 * matter.
	 */
	public static final String ORDER_SERVICE_URL = "http://ORDER-SERVICE";
	
	/**
	 * URL uses the logical name of Roless-service - upper or lower case, doesn't
	 * matter.
	 */
	public static final String ROLES_SERVICE_URL = "http://ROLES-SERVICE";
	
	/**
	 * URL uses the logical name of Shipping-service - upper or lower case, doesn't
	 * matter.
	 */
	public static final String SHIPPING_SERVICE_URL = "http://SHIPPING-SERVICE";

	/**
	 * Run the application using Spring Boot and an embedded servlet engine.
	 * 
	 * @param args
	 *            Program arguments - ignored.
	 */
	@SuppressWarnings("squid:S2095")
	public static void main(String[] args) {
		// Tell server to look for userweb-server.properties or
		// userweb-server.yml
		System.setProperty("spring.config.name", "userweb-server");
		SpringApplication.run(WebServer.class, args);
	}

	/**
	 * A customized RestTemplate that has the ribbon load balancer build in.
	 * Note that prior to the "Brixton"
	 * 
	 * @return
	 */
	@LoadBalanced
	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}

	/**
	 * The UserWebService encapsulates the interaction with the micro-service.
	 * 
	 * @return A new service instance.
	 */
	@Bean
	public UserWebService userWebService() {
		return new UserWebService(USERS_SERVICE_URL);
	}

	/**
	 * Create the controller, passing it the {@link UserWebService} to use.
	 * 
	 * @return
	 */
	@Bean
	public UserWebController userWebController() {
		return new UserWebController(userWebService());
	}
	
	/**
	 * The UserWebService encapsulates the interaction with the micro-service.
	 * 
	 * @return A new service instance.
	 */
	@Bean
	public OrderWebService orderWebService() {
		return new OrderWebService(ORDER_SERVICE_URL);
	}

	/**
	 * Create the controller, passing it the {@link UserWebService} to use.
	 * 
	 * @return
	 */
	@Bean
	public OrderWebController orderWebController() {
		return new OrderWebController(orderWebService());
	}
	
	/**
	 * The RolesWebService encapsulates the interaction with the micro-service.
	 * 
	 * @return A new service instance.
	 */
	@Bean
	public RolesWebService rolesWebService() {
		return new RolesWebService(ROLES_SERVICE_URL);
	}

	/**
	 * Create the controller, passing it the {@link RolesWebService} to use.
	 * 
	 * @return
	 */
	@Bean
	public RolesWebController rolesWebController() {
		return new RolesWebController(rolesWebService());
	}
	
	/**
	 * The ShippingWebService encapsulates the interaction with the micro-service.
	 * 
	 * @return A new service instance.
	 */
	@Bean
	public ShippingWebService shippingWebService() {
		return new ShippingWebService(SHIPPING_SERVICE_URL);
	}

	/**
	 * Create the controller, passing it the {@link ShippingWebService} to use.
	 * 
	 * @return
	 */
	@Bean
	public ShippingWebController shippingWebController() {
		return new ShippingWebController(shippingWebService());
	}
	
	/*@Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/").allowedOrigins();
            }
        };
    }*/
	
//	@Autowired
//	private Environment env;
}
