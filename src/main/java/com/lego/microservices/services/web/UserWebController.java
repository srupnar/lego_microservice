/**
 * 
 */
package com.lego.microservices.services.web;

import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import com.lego.microservices.msg.Messages;
import com.lego.microservices.persistence.dto.ErrorDto;

/**
 * Client controller, fetches User info from the microservice via
 * {@link UserWebService}.
 * 
 * @author Suresh.Rupnar
 * 
 */
@RestController
public class UserWebController {

	@Autowired
	protected UserWebService userWebService;

	protected Logger logger = Logger.getLogger(UserWebController.class.getName());

	public UserWebController(UserWebService userWebService) {
		this.userWebService = userWebService;
	}

	/**
	 * Validate user by given user Id and password
	 * 
	 * @param userID
	 * @param password
	 * @return user details, if success. Else error message
	 */
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.POST, path = "/users/validate", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
	public Object validate(@RequestBody String jsonData) {
		logger.info("userWeb-service validate() invoked:");
		// if user ID and/or password are blank
		if (StringUtils.isEmpty(jsonData)) {
			ErrorDto errorDto = new ErrorDto(Messages.getMessage("errorCode_BlankUIDPSWD"),
					Messages.getMessage("errorCodeMessage_BlankUIDPSWD"));
			return errorDto;
		}

		Object userDto = null;
		try {
			userDto = userWebService.validateUser(jsonData);
			logger.info("userWeb-service validate() found: ");
		} catch (HttpClientErrorException e) {
			return new ErrorDto(Messages.getMessage("Invalid_UserName_Password_code"),
					Messages.getMessage("Invalid_UserName_Password_Message"));
		}

		return userDto;
	}

	@CrossOrigin()
	@RequestMapping(method = RequestMethod.POST, path = "/createuser", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
	public Object createuser(@RequestBody String jsonData) {

		logger.info("userWeb-service createuser() invoked: ");
		// if user ID and/or password are blank
		if (StringUtils.isEmpty(jsonData)) {
			ErrorDto errorDto = new ErrorDto(Messages.getMessage("errorCode_BlankUIDPSWD"),
					Messages.getMessage("errorCodeMessage_BlankUIDPSWD"));
			return errorDto;
		}

		Object userDto = null;
		try {
			userDto = userWebService.createuser(jsonData);
			logger.info("userWeb-service validate() found: ");
		} catch (HttpClientErrorException e) {
			return new ErrorDto(Messages.getMessage("Invalid_UserName_Password_code"),
					Messages.getMessage("Invalid_UserName_Password_Message"));
		}

		return userDto;
	}
}
