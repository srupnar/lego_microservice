/**
 * 
 */
package com.lego.microservices.services.web;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.lego.microservices.persistence.dto.TrackingDetailsDto;
import com.lego.microservices.persistence.dto.TrackingDto;

/**
 * Hide the access to the shipping micro-service.
 * 
 * @author Suresh.Rupnar
 * 
 */
@Service
public class ShippingWebService {

	@Autowired
	@LoadBalanced
	protected RestTemplate restTemplate;

	protected String serviceUrl;
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	// private ObjectMapper objectMapper;

	protected Logger logger = Logger.getLogger(ShippingWebService.class.getName());

	/**
	 * default Constructor
	 * 
	 * @param serviceUrl
	 */
	public ShippingWebService(String serviceUrl) {
		this.serviceUrl = serviceUrl.startsWith("http") ? serviceUrl : "http://" + serviceUrl;
		// objectMapper = new ObjectMapper();
	}

	/**
	 * The RestTemplate works because it uses a custom request-factory that uses
	 * Ribbon to look-up the service to use. This method simply exists to show
	 * this.
	 */
	@PostConstruct
	public void demoOnly() {
		// Can't do this in the constructor because the RestTemplate injection
		// happens afterwards.
		logger.warning("The RestTemplate request factory is " + restTemplate.getRequestFactory().getClass());
	}

	/**
	 * Fetch list of all shipment tracking details filtered by tracking Id
	 * 
	 * @return
	 */
	@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
	public Object getShippingDetails(String trackingId) {

		logger.info("Calling getShippingDetails(String trackingId) Shipping-Web REST microservice");

		TrackingDetailsDto trackingDetailsDto = null;
		Set<TrackingDetailsDto> trackingDetailsDtoList = new HashSet();

		String[] statuslist = { "Collected", "Dispatched", "In Transit", "At Dispatch Centre", "Delivered" };

		for (int i = 0; i < 5; i++) {
			trackingDetailsDto = new TrackingDetailsDto();
			trackingDetailsDto.setStatus(statuslist[i]);
			trackingDetailsDto.setDate(dateFormat.format(new Date(116, 11, 12 + i)));

			trackingDetailsDtoList.add(trackingDetailsDto);
		}

		TrackingDto trackingDto = new TrackingDto();
		trackingDto.setTrackingId(trackingId);
		trackingDto.setStatus("In Transit");
		trackingDto.setTrackingDetailsDto(trackingDetailsDtoList);

		return trackingDto;
	}

}
