/**
 * 
 */
package com.lego.microservices.services.web;

import java.io.IOException;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lego.microservices.exceptions.GenericException;
import com.lego.microservices.exceptions.util.ExceptionHandlerResource;
import com.lego.microservices.persistence.dto.ErrorDto;
import com.lego.microservices.persistence.dto.RolesDto;
import com.lego.microservices.services.util.MyResponseErrorHandler;
import com.lego.microservices.services.util.RestUtil;

/**
 * Hide the access to the microservice inside this local service.
 * 
 * @author Suresh.Rupnar
 * 
 */
@Service
public class RolesWebService {

	@Autowired
	@LoadBalanced
	protected RestTemplate restTemplate;

	protected String serviceUrl;
	private ObjectMapper objectMapper;

	protected Logger logger = Logger.getLogger(RolesWebService.class.getName());

	/**
	 * default Constructor
	 * 
	 * @param serviceUrl
	 */
	public RolesWebService(String serviceUrl) {
		this.serviceUrl = serviceUrl.startsWith("http") ? serviceUrl : "http://" + serviceUrl;
		objectMapper = new ObjectMapper();
	}

	/**
	 * The RestTemplate works because it uses a custom request-factory that uses
	 * Ribbon to look-up the service to use. This method simply exists to show
	 * this.
	 */
	@PostConstruct
	public void demoOnly() {
		// Can't do this in the constructor because the RestTemplate injection
		// happens afterwards.
		logger.warning("The RestTemplate request factory is " + restTemplate.getRequestFactory().getClass());
	}
	
	/**
	 * fetch the role details w.r.t role id
	 * @param rolesID
	 * @return
	 */
	public Object getRoles(int rolesID) {

		logger.info("Calling Roles-web service getRoles(int rolesID) REST microservice");

		restTemplate.setErrorHandler(new MyResponseErrorHandler());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<String> request = new HttpEntity<String>(headers);
		ResponseEntity<String> response = restTemplate.exchange(serviceUrl + "/roles/" + rolesID, HttpMethod.GET,
				request, String.class);
		String responseBody = response.getBody();
		try {
			if (RestUtil.isError(response.getStatusCode())) {
				ExceptionHandlerResource error = objectMapper.readValue(responseBody, ExceptionHandlerResource.class);
				return new ErrorDto(error.getMessage().substring(0, 3), error.getMessage().substring(4));
			} else {
				return objectMapper.readValue(responseBody, RolesDto.class);
			}
		} catch (IOException e) {
			throw new GenericException("", e);
		}
	}
	
	/**
	 * fetch roles by user Id
	 * @param userid
	 * @return role
	 */
	public Object getRolesByuserId(String userid) {

		logger.info("Calling Roles-web service getRolesByuserId(String userid) REST microservice");

		restTemplate.setErrorHandler(new MyResponseErrorHandler());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<String> request = new HttpEntity<String>(headers);
		ResponseEntity<String> response = restTemplate.exchange(serviceUrl + "/roles/user/" + userid, HttpMethod.GET,
				request, String.class);
		String responseBody = response.getBody();
		try {
			if (RestUtil.isError(response.getStatusCode())) {
				ExceptionHandlerResource error = objectMapper.readValue(responseBody, ExceptionHandlerResource.class);
				return new ErrorDto(error.getMessage().substring(0, 3), error.getMessage().substring(4));
			} else {
				return objectMapper.readValue(responseBody, RolesDto.class);
			}
		} catch (IOException e) {
			throw new GenericException("", e);
		}
	}
}
