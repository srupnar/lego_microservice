/**
 * 
 */
package com.lego.microservices.services.users;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

import com.lego.microservices.persistence.repository.UserRepository;
import com.lego.microservices.services.config.DatabaseConfig;

/**
 * Run as a micro-service, registering with the Discovery Server (Eureka).
 * <p>
 * Note that the configuration for this application is imported from
 * {@link DatabaseConfig}. This is a deliberate separation of concerns.
 * 
 * @author Suresh.Rupnar
 * 
 */
@EnableAutoConfiguration
@EnableDiscoveryClient
@Import(DatabaseConfig.class)
public class UsersServer {
	
	@Autowired
	protected UserRepository userRepository;

	protected Logger logger = Logger.getLogger(UsersServer.class.getName());

	/**
	 * Run the application using Spring Boot and an embedded servlet engine.
	 * 
	 * @param args
	 *            Program arguments - ignored.
	 */
	@SuppressWarnings("squid:S2095")
	public static void main(String[] args) {
		// Tell server to look for accounts-server.properties or
		// accounts-server.yml
		System.setProperty("spring.config.name", "users-server");

		SpringApplication.run(UsersServer.class, args);

	}

}
