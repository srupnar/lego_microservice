/**
 * 
 */
package com.lego.microservices.services.users;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lego.microservices.exceptions.CannotPerformOperationException;
import com.lego.microservices.exceptions.GenericException;
import com.lego.microservices.exceptions.InvalidHashException;
import com.lego.microservices.exceptions.PasswordMismatchException;
import com.lego.microservices.exceptions.RoleNotFoundException;
import com.lego.microservices.exceptions.UserNotFoundException;
import com.lego.microservices.persistence.Roles;
import com.lego.microservices.persistence.Users;
import com.lego.microservices.persistence.dto.RolesDto;
import com.lego.microservices.persistence.dto.UsersDto;
import com.lego.microservices.persistence.repository.RolesRepository;
import com.lego.microservices.persistence.repository.UserRepository;
import com.lego.microservices.services.util.PasswordUtil;

/**
 * A RESTFul controller for accessing user information.
 * 
 * @author Suresh.Rupnar
 * 
 */
@RestController
@Transactional
public class UsersController {

	protected Logger logger = Logger.getLogger(UsersController.class.getName());
	protected UserRepository userRepository;
	protected RolesRepository rolesRepository;

	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	@PersistenceContext
	private EntityManager em;

	/**
	 * Create an instance plugging in the respository of Users.
	 * 
	 * @param userRepository
	 *            An user repository implementation.
	 */
	@Autowired
	public UsersController(UserRepository userRepository, RolesRepository rolesRepository) {
		this.userRepository = userRepository;
		this.rolesRepository = rolesRepository;
		logger.info("UserRepository says system has " + userRepository.countUsers() + " users");
	}

	/**
	 * Validate user with the specified user Id and password.
	 * 
	 * @param userID
	 *            A varchar, user identifier.
	 * @param password
	 *            A varchar, user password.
	 * @return The user if found.
	 * @throws UserNotFoundException
	 *             If the user ID is not recognized.
	 */
	@RequestMapping(method = RequestMethod.POST, path = "/users/validate", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
	public UsersDto validate(@RequestBody String jsonData) {
		Users users = null;
		try {
			HashMap<String, String> map = new HashMap<String, String>();
			JSONObject jObject = new JSONObject(jsonData);
			Iterator<?> keys = jObject.keys();

			while (keys.hasNext()) {
				String key = (String) keys.next();
				String value = jObject.getString(key);
				map.put(key, value);
			}
			logger.info("users-service validate() invoked: " + map.get("userId"));
			users = userRepository.findByUserId(map.get("userId"));
			logger.info("users-service validate() found: " + users);

			if (users == null)
				throw new UserNotFoundException(map.get("userId"), " ");

			boolean flag;
			try {
				flag = PasswordUtil.verifyPassword(map.get("Password").toCharArray(), users.getHash(), users.getSalt());
			} catch (CannotPerformOperationException e) {
				throw new CannotPerformOperationException("Cannot verify password");
			} catch (InvalidHashException e) {
				throw new InvalidHashException("Cannot verify password");
			}
			if (!flag)
				throw new PasswordMismatchException();

		} catch (Exception e) {
			throw new InvalidHashException("Cannot verify password");
		}

		return fillUserDto(users);
	}

	/**
	 * create User
	 * 
	 * @param jsonData
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, path = "/createuser", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
	public UsersDto createuser(@RequestBody String jsonData) {
		HashMap<String, String> map = new HashMap<String, String>();

		try {
			JSONObject jObject = new JSONObject(jsonData);
			Iterator<?> keys = jObject.keys();

			while (keys.hasNext()) {
				String key = (String) keys.next();
				String value = jObject.getString(key);
				map.put(key, value);
			}

			logger.info("users-service create() invoked: " + map.get("userId"));
			Users users = userRepository.findByUserId(map.get("userId"));
			logger.info("users-service validate() found: " + users);

			if (users == null) {
				String password = map.get("Password");
				String hashsalt = PasswordUtil.getHashSalt(password.toCharArray());				
				String salt = hashsalt.substring(0, hashsalt.indexOf("$*$"));
				
				users = new Users();
				users.setUserId(map.get("userId"));
				users.setHash(hashsalt.substring(salt.length() + 3));
				users.setSalt(salt);
				users.setRoleValue(map.get("attrValue"));

				Roles role = rolesRepository.findByRoleId(Integer.parseInt(map.get("roleId")));
				if (role == null) {
					/*Roles newrole = new Roles();
					newrole.setAttrName(map.get("attrName"));
					newrole.setRoleName(map.get("roleName"));
					
					users.setRoles(newrole);
					em.persist(newrole);*/
					throw new RoleNotFoundException(Integer.parseInt(map.get("roleId")));
				} else {
					users.setRoles(role);
				}
				users.setCreatedBy("System");
				users.setCreatedTime(new Date());
				users.setLastLoginTime(new Date());
				users.setUpdatedBy("System");
				users.setUpdatedTime(new Date());
				em.persist(users);
				
				return fillUserDto(users);
			} else {
				throw new UserNotFoundException(users.getUserId());
			}
		} catch (Exception e) {
			throw new GenericException("", e);
		}

	}

	private UsersDto fillUserDto(Users user) {
		UsersDto userDto = new UsersDto();
		userDto.setId(user.getId());
		userDto.setHash(user.getHash());
		userDto.setSalt(user.getSalt());
		userDto.setCreatedBy(user.getCreatedBy());
		userDto.setCreatedTime(dateFormat.format(user.getCreatedTime()));
		userDto.setLastLoginTime(dateFormat.format(user.getLastLoginTime()));
		userDto.setUserId(user.getUserId());
		userDto.setUpdatedBy(user.getUpdatedBy());
		userDto.setUpdatedTime(dateFormat.format(user.getUpdatedTime()));

		Roles roles = user.getRoles();
		RolesDto roleDto = new RolesDto();
		roleDto.setAttrName(roles.getAttrName());
		roleDto.setAttrValue(user.getRoleValue());
		roleDto.setRoleId(roles.getRoleId());
		roleDto.setRoleName(roles.getRoleName());

		userDto.setRoles(roleDto);

		return userDto;
	}
}
