/**
 * 
 */
package com.lego.microservices.persistence.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.lego.microservices.persistence.Shipmentdetails;

/**
 * @author Suresh.Rupnar
 *
 */
@JsonRootName("UpdateOrder")
public class UpdateOrderDto {

	private String status;
	private String statusDescription;
	private String createdBy;
	private String attrName;
	private String attrValue;
	private String description;
	private Long orders;
	private Integer linedetails;

	private Shipmentdetails shipmentdetails;

	/**
	 * 
	 */
	public UpdateOrderDto() {
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the attrName
	 */
	public String getAttrName() {
		return attrName;
	}

	/**
	 * @param attrName
	 *            the attrName to set
	 */
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	/**
	 * @return the attrValue
	 */
	public String getAttrValue() {
		return attrValue;
	}

	/**
	 * @param attrValue
	 *            the attrValue to set
	 */
	public void setAttrValue(String attrValue) {
		this.attrValue = attrValue;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the orders
	 */
	public Long getOrders() {
		return orders;
	}

	/**
	 * @param orders
	 *            the orders to set
	 */
	public void setOrders(Long orders) {
		this.orders = orders;
	}

	/**
	 * @return the linedetails
	 */
	public Integer getLinedetails() {
		return linedetails;
	}

	/**
	 * @param linedetails
	 *            the linedetails to set
	 */
	public void setLinedetails(Integer linedetails) {
		this.linedetails = linedetails;
	}

	/**
	 * @return the shipmentdetails
	 */
	public Shipmentdetails getShipmentdetails() {
		return shipmentdetails;
	}

	/**
	 * @param shipmentdetails
	 *            the shipmentdetails to set
	 */
	public void setShipmentdetails(Shipmentdetails shipmentdetails) {
		this.shipmentdetails = shipmentdetails;
	}

	/**
	 * @return the statusDescription
	 */
	public String getStatusDescription() {
		return statusDescription;
	}

	/**
	 * @param statusDescription
	 *            the statusDescription to set
	 */
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

}
