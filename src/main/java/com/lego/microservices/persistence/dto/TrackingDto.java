/**
 * 
 */
package com.lego.microservices.persistence.dto;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * @author Suresh.Rupnar
 *
 */
@JsonRootName("Tracking")
public class TrackingDto {

	private String trackingId;
	private String status;
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Set<TrackingDetailsDto> trackingDetailsDto = new HashSet();

	/**
	 * default constructor
	 */
	public TrackingDto() {
	}

	/**
	 * @return the trackingId
	 */
	public String getTrackingId() {
		return trackingId;
	}

	/**
	 * @param trackingId
	 *            the trackingId to set
	 */
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the trackingDetailsDto
	 */
	public Set<TrackingDetailsDto> getTrackingDetailsDto() {
		return trackingDetailsDto;
	}

	/**
	 * @param trackingDetailsDto
	 *            the trackingDetailsDto to set
	 */
	public void setTrackingDetailsDto(Set<TrackingDetailsDto> trackingDetailsDto) {
		this.trackingDetailsDto = trackingDetailsDto;
	}

}
