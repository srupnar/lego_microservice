package com.lego.microservices.persistence.dto;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("Order")
public class OrderDto {

	private long orderId;
	private String postatus;
	private String updatedBy;
	private String updatedDate;
	private String createdBy;
	private String createdDate;
	private String warehouseCity;
	private String supplierAddress1;
	private String supplierAddress2;
	private String warehouseAddress1;
	private String warehouseAddress2;
	private String warehouseCountry;
	private Integer supplierId;
	private String supplierName;
	private String supplierCity;
	private String supplierCountry;
	private String brokerId;
	private String description;
	private Double poValue;
	private String statusdescription;
	private String currency;
	private String expectedDeliveryDate;

	// private Set<OrderstatusDto> orderstatuses = new
	// HashSet<OrderstatusDto>();
	private Set<LinedetailsDto> linedetailsesDto = new HashSet<LinedetailsDto>(0);
	// private Set<ShipmentdetailsDto> shipmentdetailsesDto = new
	// HashSet<ShipmentdetailsDto>(0);

	public OrderDto() {
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public String getPostatus() {
		return postatus;
	}

	public void setPostatus(String postatus) {
		this.postatus = postatus;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;

	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		// dateFormat.setTimeZone(TimeZone.getDefault());
		this.createdDate = createdDate;

	}

	public String getWarehouseCity() {
		return warehouseCity;
	}

	public void setWarehouseCity(String warehouseCity) {
		this.warehouseCity = warehouseCity;
	}

	public String getSupplierAddress1() {
		return supplierAddress1;
	}

	public void setSupplierAddress1(String supplierAddress1) {
		this.supplierAddress1 = supplierAddress1;
	}

	public String getSupplierAddress2() {
		return supplierAddress2;
	}

	public void setSupplierAddress2(String supplierAddress2) {
		this.supplierAddress2 = supplierAddress2;
	}

	public String getWarehouseAddress1() {
		return warehouseAddress1;
	}

	public void setWarehouseAddress1(String warehouseAddress1) {
		this.warehouseAddress1 = warehouseAddress1;
	}

	public String getWarehouseAddress2() {
		return warehouseAddress2;
	}

	public void setWarehouseAddress2(String warehouseAddress2) {
		this.warehouseAddress2 = warehouseAddress2;
	}

	public String getWarehouseCountry() {
		return warehouseCountry;
	}

	public void setWarehouseCountry(String warehouseCountry) {
		this.warehouseCountry = warehouseCountry;
	}

	public Integer getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierCity() {
		return supplierCity;
	}

	public void setSupplierCity(String supplierCity) {
		this.supplierCity = supplierCity;
	}

	public String getSupplierCountry() {
		return supplierCountry;
	}

	public void setSupplierCountry(String supplierCountry) {
		this.supplierCountry = supplierCountry;
	}

	public String getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(String brokerId) {
		this.brokerId = brokerId;
	}

	/*	*//**
			 * @return the orderstatuses
			 */
	/*
	 * public Set<OrderstatusDto> getOrderstatuses() { return orderstatuses; }
	 * 
	 *//**
		 * @param orderstatuses
		 *            the orderstatuses to set
		 *//*
		 * public void setOrderstatuses(Set<OrderstatusDto> orderstatuses) {
		 * this.orderstatuses = orderstatuses; }
		 */

	/**
	 * @return the linedetailsesDto
	 */
	public Set<LinedetailsDto> getLinedetailsesDto() {
		return linedetailsesDto;
	}

	/**
	 * @param linedetailsesDto
	 *            the linedetailsesDto to set
	 */
	public void setLinedetailsesDto(Set<LinedetailsDto> linedetailsesDto) {
		this.linedetailsesDto = linedetailsesDto;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the poValue
	 */
	public Double getPoValue() {
		return poValue;
	}

	/**
	 * @param poValue
	 *            the poValue to set
	 */
	public void setPoValue(Double poValue) {
		this.poValue = poValue;
	}

	/**
	 * @return the statusdescription
	 */
	public String getStatusdescription() {
		return statusdescription;
	}

	/**
	 * @param statusdescription
	 *            the statusdescription to set
	 */
	public void setStatusdescription(String statusdescription) {
		this.statusdescription = statusdescription;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the expectedDeliveryDate
	 */
	public String getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}

	/**
	 * @param expectedDeliveryDate
	 *            the expectedDeliveryDate to set
	 */
	public void setExpectedDeliveryDate(String expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}

	/**
	 * @return the supplierName
	 */
	public String getSupplierName() {
		return supplierName;
	}

	/**
	 * @param supplierName
	 *            the supplierName to set
	 */
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

}
