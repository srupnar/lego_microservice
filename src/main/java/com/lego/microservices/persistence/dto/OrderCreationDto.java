package com.lego.microservices.persistence.dto;

import java.util.List;

public class OrderCreationDto {
	private long orderId;
	private String expectedDeliveryDate;
	private List<Integer> lineId;

	/**
	 * @return the orderId
	 */
	public long getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId
	 *            the orderId to set
	 */
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	/**
	 * @return the lineId
	 */
	public List<Integer> getLineId() {
		return lineId;
	}

	/**
	 * @param lineId
	 *            the lineId to set
	 */
	public void setLineId(List<Integer> lineId) {
		this.lineId = lineId;
	}

	/**
	 * @return the expectedDeliveryDate
	 */
	public String getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}

	/**
	 * @param expectedDeliveryDate
	 *            the expectedDeliveryDate to set
	 */
	public void setExpectedDeliveryDate(String expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}

}
