package com.lego.microservices.persistence.dto;

import java.util.List;

public class StaticsDtoMain {
private String date;
private List<StaticsDto> staticsDto;
/**
 * @return the date
 */
public String getDate() {
	return date;
}
/**
 * @param date the date to set
 */
public void setDate(String date) {
	this.date = date;
}
/**
 * @return the staticsDto
 */
public List<StaticsDto> getStaticsDto() {
	return staticsDto;
}
/**
 * @param staticsDto the staticsDto to set
 */
public void setStaticsDto(List<StaticsDto> staticsDto) {
	this.staticsDto = staticsDto;
}

	
}
