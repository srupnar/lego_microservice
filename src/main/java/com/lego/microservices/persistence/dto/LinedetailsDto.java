/**
 * 
 */
package com.lego.microservices.persistence.dto;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * @author katre.v
 *
 */
@JsonRootName("Linedetail")
public class LinedetailsDto {

	private Integer lineId;
	private String createdAt;
	private String mouldNo;
	private Integer artNo;
	private Integer quantity;
	private String status;
	private String lineDescription;
	private Double unitPrice;
	// private OrderDto orderDto;
	private Set<OrderstatusDto> orderStatusesDto = new HashSet<OrderstatusDto>(0);

	/**
	 * default Contructor
	 */
	public LinedetailsDto() {
	}

	public Integer getLineId() {
		return lineId;
	}

	public void setLineId(Integer lineId) {
		this.lineId = lineId;
	}

	// /* /**
	// * @return the orderDto
	// */
	// public OrderDto getOrderDto() {
	// return orderDto;
	// }
	//
	// *//**
	// * @param orderDto
	// * the orderDto to set
	// *//*
	// public void setOrderDto(OrderDto orderDto) {
	// this.orderDto = orderDto;
	// }

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getMouldNo() {
		return mouldNo;
	}

	public void setMouldNo(String mouldNo) {
		this.mouldNo = mouldNo;
	}

	public Integer getArtNo() {
		return artNo;
	}

	public void setArtNo(Integer artNo) {
		this.artNo = artNo;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the lineDescription
	 */
	public String getLineDescription() {
		return lineDescription;
	}

	/**
	 * @param lineDescription
	 *            the lineDescription to set
	 */
	public void setLineDescription(String lineDescription) {
		this.lineDescription = lineDescription;
	}

	/**
	 * @return the unitPrice
	 */
	public Double getUnitPrice() {
		return unitPrice;
	}

	/**
	 * @param unitPrice
	 *            the unitPrice to set
	 */
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	/**
	 * @return the orderStatusesDto
	 */
	public Set<OrderstatusDto> getOrderStatusesDto() {
		return orderStatusesDto;
	}

	/**
	 * @param orderStatusesDto
	 *            the orderStatusesDto to set
	 */
	public void setOrderStatusesDto(Set<OrderstatusDto> orderStatusesDto) {
		this.orderStatusesDto = orderStatusesDto;
	}

}
