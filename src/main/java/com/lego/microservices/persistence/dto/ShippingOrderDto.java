package com.lego.microservices.persistence.dto;

import java.util.HashSet;
import java.util.Set;

public class ShippingOrderDto {

	private long orderId;
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Set<LineIdDto> linedetailsesDto = new HashSet();
	/**
	 * @return the orderId
	 */
	public long getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	/**
	 * @return the linedetailsesDto
	 */
	public Set<LineIdDto> getLinedetailsesDto() {
		return linedetailsesDto;
	}
	/**
	 * @param linedetailsesDto the linedetailsesDto to set
	 */
	public void setLinedetailsesDto(Set<LineIdDto> linedetailsesDto) {
		this.linedetailsesDto = linedetailsesDto;
	}
	
}
