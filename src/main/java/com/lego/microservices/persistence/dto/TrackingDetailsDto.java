/**
 * 
 */
package com.lego.microservices.persistence.dto;

import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * @author Suresh.Rupnar
 *
 */
@JsonRootName("TrackingDetails")
public class TrackingDetailsDto {

	private String status;
	private String date;

	/**
	 * default constructor
	 */
	public TrackingDetailsDto() {
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

}
