/**
 * 
 */
package com.lego.microservices.persistence.dto;

import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * @author katre.v
 *
 */
@JsonRootName("OrderStatus")
public class OrderstatusDto {

	private Integer id;
	private Long orderId;
	private Integer lineId;
	private String status;
	private String createdBy;
	private String createdDate;
	private String attrName;
	private String attrValue;
	private String description;
	private String  expectedDeliveryDate;

//	private LinedetailsDto linedetailsDto;
//	private OrderDto ordersDto;

	/**
	 * default constructor
	 */
	
	
	public OrderstatusDto() {
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Integer getLineId() {
		return lineId;
	}

	public void setLineId(Integer lineId) {
		this.lineId = lineId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
		
	}

	public String getAttrName() {
		return attrName;
	}

	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	public String getAttrValue() {
		return attrValue;
	}

	public void setAttrValue(String attrValue) {
		this.attrValue = attrValue;
	}

//	/**
//	 * @return the linedetailsDto
//	 */
//	public LinedetailsDto getLinedetailsDto() {
//		return linedetailsDto;
//	}
//
//	/**
//	 * @param linedetailsDto
//	 *            the linedetailsDto to set
//	 */
//	public void setLinedetailsDto(LinedetailsDto linedetailsDto) {
//		this.linedetailsDto = linedetailsDto;
//	}
//
//	/**
//	 * @return the ordersDto
//	 */
//	public OrderDto getOrdersDto() {
//		return ordersDto;
//	}
//
//	/**
//	 * @param ordersDto
//	 *            the ordersDto to set
//	 */
//	public void setOrdersDto(OrderDto ordersDto) {
//		this.ordersDto = ordersDto;
//	}

	/**
	 * @return the expectedDeliveryDate
	 */
	public String getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}

	/**
	 * @param expectedDeliveryDate the expectedDeliveryDate to set
	 */
	public void setExpectedDeliveryDate(String expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
