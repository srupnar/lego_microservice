/**
 * 
 */
package com.lego.microservices.persistence.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.lego.microservices.services.users.UsersController;

/**
 * User DTO - used to interact with the {@link UsersController}.
 * 
 * @author Suresh.Rupnar
 *
 */
@JsonRootName("User")
public class UsersDto {

	private Integer id;
	private RolesDto rolesDto;
	private String userId;
	private String salt;
	private String hash;
	private String lastLoginTime;
	private String createdTime;
	private String createdBy;
	private String updatedTime;
	private String updatedBy;

	/**
	 * Default constructor for JPA only.
	 */
	public UsersDto() {
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the rolesDto
	 */
	public RolesDto getRolesDto() {
		return rolesDto;
	}

	/**
	 * @param rolesDto
	 *            the rolesDto to set
	 */
	public void setRoles(RolesDto rolesDto) {
		this.rolesDto = rolesDto;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the salt
	 */
	public String getSalt() {
		return salt;
	}

	/**
	 * @param salt
	 *            the salt to set
	 */
	public void setSalt(String salt) {
		this.salt = salt;
	}

	/**
	 * @return the hash
	 */
	public String getHash() {
		return hash;
	}

	/**
	 * @param hash
	 *            the hash to set
	 */
	public void setHash(String hash) {
		this.hash = hash;
	}

	/**
	 * @return the lastLoginTime
	 */
	public String getLastLoginTime() {
		return lastLoginTime;
	}

	/**
	 * @param lastLoginTime
	 *            the lastLoginTime to set
	 */
	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;

	}

	/**
	 * @return the createdTime
	 */
	public String getCreatedTime() {
		return createdTime;
	}

	/**
	 * @param createdTime
	 *            the createdTime to set
	 */
	public void setCreatedTime(String createdTime) {

		this.createdTime = createdTime;

	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the updatedTime
	 */
	public String getUpdatedTime() {
		return updatedTime;
	}

	/**
	 * @param updatedTime
	 *            the updatedTime to set
	 */
	public void setUpdatedTime(String updatedTime) {

		this.updatedTime = updatedTime;
	}

	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy
	 *            the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UsersDto [id=" + id + ", userId=" + userId + ", salt=" + salt + ", hash=" + hash + ", lastLoginTime="
				+ lastLoginTime + ", createdTime=" + createdTime + ", createdBy=" + createdBy + ", updatedTime="
				+ updatedTime + ", updatedBy=" + updatedBy + "]";
	}

}
