/**
 * 
 */
package com.lego.microservices.persistence.dto;

import java.util.Date;
/**
 * @author katre.v
 *
 */
public class GenericOrderSearchDto {

	private long orderId;
	private String status;
	private String description;
	private String expectedDeliveryDate;
	//private String artNo;
	
	public GenericOrderSearchDto() {
		// default constructor
	}

	/**
	 * @return the orderId
	 */
	public long getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId
	 *            the orderId to set
	 */
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the expectedDeliveryDate
	 */
	public String getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}

	/**
	 * @param expectedDeliveryDate the expectedDeliveryDate to set
	 */
	public void setExpectedDeliveryDate(String expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}
/*	*//**
	 * @return the artNo
	 *//*
	public String getArtNo() {
		return artNo;
	}

	*//**
	 * @param artNo
	 *            the artNo to set
	 *//*
	public void setArtNo(String artNo) {
		this.artNo = artNo;
	}*/

}
