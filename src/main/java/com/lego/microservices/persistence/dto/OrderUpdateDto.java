package com.lego.microservices.persistence.dto;

public class OrderUpdateDto {

	private String orderId;
	private String lineId;
	private Integer shipmentTrackingId;
	
	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * @return the lineId
	 */
	public String getLineId() {
		return lineId;
	}
	/**
	 * @param lineId the lineId to set
	 */
	public void setLineId(String lineId) {
		this.lineId = lineId;
	}
	/**
	 * @return the shipmentTrackingId
	 */
	public Integer getShipmentTrackingId() {
		return shipmentTrackingId;
	}
	/**
	 * @param shipmentTrackingId the shipmentTrackingId to set
	 */
	public void setShipmentTrackingId(Integer shipmentTrackingId) {
		this.shipmentTrackingId = shipmentTrackingId;
	}
	
	
}
