package com.lego.microservices.persistence.dto;

public class LineIdDto {
	private Integer lineId;
	private Integer trackingId;
	/**
	 * @return the lineId
	 */
	public Integer getLineId() {
		return lineId;
	}
	/**
	 * @param lineId the lineId to set
	 */
	public void setLineId(Integer lineId) {
		this.lineId = lineId;
	}
	/**
	 * @return the trackingId
	 */
	public Integer getTrackingId() {
		return trackingId;
	}
	/**
	 * @param trackingId the trackingId to set
	 */
	public void setTrackingId(Integer trackingId) {
		this.trackingId = trackingId;
	}
	
	
}
