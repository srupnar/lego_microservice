package com.lego.microservices.persistence.dto;

public class StaticsDto 
{
private String status;
private Double count;
private String percentage;
/**
 * @return the status
 */
public String getStatus() {
	return status;
}
/**
 * @param status the status to set
 */
public void setStatus(String status) {
	this.status = status;
}
/**
 * @return the count
 */
public Double getCount() {
	return count;
}
/**
 * @param count the count to set
 */
public void setCount(Double count) {
	this.count = count;
}
/**
 * @return the percentage
 */
public String getPercentage() {
	return percentage;
}
/**
 * @param percentage the percentage to set
 */
public void setPercentage(String percentage) {
	this.percentage = percentage;
}

}
