/**
 * 
 */
package com.lego.microservices.persistence.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.lego.microservices.persistence.Users;

/**
 * Repository for User data implemented using Spring Data JPA.
 * @author Suresh.Rupnar
 *
 */
public interface UserRepository extends Repository<Users, Integer> {
	
	/**
	 * Find an user with the specified user ID.
	 *
	 * @param userID
	 * @return The user if found, null otherwise.
	 */
	public Users findByUserId(String userID);
	
	/**
	 * Fetch the number of users known to the system.
	 * 
	 * @return The number of users.
	 */
	@Query("SELECT count(*) from Users")
	public int countUsers();

}
