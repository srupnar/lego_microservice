/**
 * 
 */
package com.lego.microservices.persistence.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.lego.microservices.persistence.Linedetails;

/**
 * @author Suresh.Rupnar
 *
 */
public interface LinedetailsRepository extends Repository<Linedetails, Integer> {
	
	@Query(value = "SELECT ld FROM Linedetails ld WHERE ld.orders.orderId = ?1 and ld.lineId = ?2 ")
	public Linedetails findbyLinedetails(Long orderid, Integer lineid);
}
