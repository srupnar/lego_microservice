package com.lego.microservices.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.lego.microservices.persistence.Orderstatus;

/**
 * Repository for Orderstatus
 * @author Suresh.Rupnar
 *
 */
public interface OrderStatusRepository extends CrudRepository<Orderstatus, Integer> {

	@Query(value = "SELECT od FROM Orderstatus od WHERE od.orders.orderId = ?1 and od.linedetails.lineId = ?2 order by od.createdDate asc ")
	public List<Orderstatus> findbyOrderStatus(Long orderid, Integer lineid);
}
