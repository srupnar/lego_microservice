package com.lego.microservices.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lego.microservices.persistence.Roles;

/**
 * Repository for Roles persistent entity
 * @author Suresh.Rupnar
 *
 */
public interface RolesRepository extends JpaRepository<Roles, Integer> {

	/**
	 * Find an users with the specified role ID.
	 *
	 * @param roleID
	 * @return users list , null otherwise.
	 */
	public Roles findByRoleId(Integer roleID);

}
