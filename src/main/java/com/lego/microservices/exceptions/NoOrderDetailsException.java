package com.lego.microservices.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.lego.microservices.msg.Messages;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NoOrderDetailsException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public NoOrderDetailsException(String message) {
		super(Messages.getMessage("errorCode_NoOrderFound") + " " + Messages.getMessage("errorMessage_NoOrderFound"));
	}

	public NoOrderDetailsException(String message, Throwable source) {
		super(Messages.getMessage("errorCode_NoOrderFound") + " " + Messages.getMessage("errorMessage_NoOrderFound"), source);
	}

}