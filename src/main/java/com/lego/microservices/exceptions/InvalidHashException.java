package com.lego.microservices.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.lego.microservices.msg.Messages;

@SuppressWarnings("serial")
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class InvalidHashException extends RuntimeException {
	public InvalidHashException(String message) {
		super(Messages.getMessage("Technical_Error_Code") + " " + message);
	}

	public InvalidHashException(String message, Throwable source) {
		super(Messages.getMessage("Technical_Error_Code") + " " + message, source);
	}
}