/**
 * 
 */
package com.lego.microservices.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.lego.microservices.msg.Messages;

/**
 * While user creation, if user is already exist by provided user id. Then throw this exception
 * @author Suresh.Rupnar
 *
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class UserAlreadyExistException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public UserAlreadyExistException(String userID) {
		super(Messages.getMessage("User_Already_Exist_Code") + " " + Messages.getMessage("User_Already_Exist_Message") + userID);
	}
}
