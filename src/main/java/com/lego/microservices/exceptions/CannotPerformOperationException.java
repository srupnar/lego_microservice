package com.lego.microservices.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.lego.microservices.msg.Messages;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class CannotPerformOperationException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public CannotPerformOperationException(String message) {
		super(Messages.getMessage("Technical_Error_Code") + " " + message);
	}

	public CannotPerformOperationException(String message, Throwable source) {
		super(Messages.getMessage("Technical_Error_Code") + " " + message, source);
	}

}