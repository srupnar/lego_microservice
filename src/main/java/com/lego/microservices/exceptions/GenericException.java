package com.lego.microservices.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.lego.microservices.msg.Messages;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class GenericException extends RuntimeException
{
	private static final long serialVersionUID = 1L;

	public GenericException(String message, Throwable source) {
		super(Messages.getMessage("Technical_Error_Code") + " " + message + " " + source.getMessage(), source);
	}

}