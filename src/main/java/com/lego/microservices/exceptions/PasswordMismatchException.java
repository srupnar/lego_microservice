package com.lego.microservices.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.lego.microservices.msg.Messages;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class PasswordMismatchException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public PasswordMismatchException() {
		super(Messages.getMessage("Wrong_Password_Code") + " " + Messages.getMessage("Wrong_Password_Message"));
	}
}
