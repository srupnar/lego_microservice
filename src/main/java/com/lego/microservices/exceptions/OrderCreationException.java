package com.lego.microservices.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.lego.microservices.msg.Messages;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class OrderCreationException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	
	public OrderCreationException(String message) {
		super(Messages.getMessage("Order_details_are_null") + " " + message);
	}

	public OrderCreationException(String message, Throwable source) {
		super(Messages.getMessage("Technical_Error_Code") + " " + message, source);
	}

}